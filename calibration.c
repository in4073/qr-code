/*------------------------------------------------------------
* AUTHOR: Eric Camellini
*------------------------------------------------------------
*/
#include "calibration.h"
#include "filter.h"
#include "debug.h"
#include "main.h"
#include "string.h"
#include "pidtester.h"
#include "fixedpoint.h"
#include "pidcontroller.h"


static bool isCalibrated = 0;

int sensor_offsets[6] = {0};
int sensorBaseOffsets[6] = { 500, 500, 400, 400, 400, 400};


/*
 * Author: Eric Camellini
 * Takes the most recent filtered sensor values and puts them in the sensor_offsets
 * array that contains the offsets to take into account the calibration when using
 * sensor values.
 */
void RunCalibration(void){
    int i,j;
    int sumOfOutputs = 0;
    int oldCalibrationVal; /* Necessary for possible recalibration */

    for(i = 0; i < 6; i++){
    	sumOfOutputs = 0;
    	oldCalibrationVal = sensor_offsets[i];
    	for(j = 0; j < CAL_NUM_SAMPLES; j++){
			#if DATA_SRC == SENSOR_DATA
    			sumOfOutputs += sensors[i][SensorWriteIndex];
			#else
    			sumOfOutputs += filterOutput[i][j] + oldCalibrationVal;
			#endif
	    }
	    sensor_offsets[i] = sumOfOutputs / CAL_NUM_SAMPLES;
    }
		print_offsets();
    isCalibrated = true;
}


/*
 * Author: Eric Camellini
 * Prints the current sensor offsets.
 */
void print_offsets(void){
  int i;
  debug_printf("Current offsets: ");
  for(i = 0; i < 6; i++){
	#if DATA_SRC == SENSOR_DATA
	  debug_printf("%d ", sensor_offsets[i]);
	#else
	  debug_printf("%d ", fp_int_i(sensor_offsets[i],FILTER_PREC));
	#endif
  }
}


/*
 * Author: Eric Camellini
 * Returns false if all the offsets are 0.
 */
bool is_calibrated(void){
  return isCalibrated;
}

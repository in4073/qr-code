#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include "../main.h"
#include "../protocol.h"
#include "logger.h"
#include "../uart.h"

/*
 * PLEASE NOTE!
 *
 * This was the first version of the logger and has been left alone for at least 1.5 months.
 * This logger was created with a wrong kind of logger in mind.
 * For this reason this code is not officially assinged an autor.
 * The original author was Hajo Kleingeld
 */


/* Page 58 of X32 user manual states:
--------------------------
	Note: malloc can only allocate blocks of memory within a user defined memory space. To
	use malloc, statically allocate a memory block with the name malloc_memory. In addition,
	an integer named malloc_memory_size is required which should hold the size of the memory
	block. The following code allocates 1KB for malloc to use:
	char malloc_memory[1024];
	malloc_memory_size = 1024;
-----------------------------
 	In the ES we are creating no other Dynamic allocation is sued, so its easier and more effecient
 	to just create a big statc block on the heap.
*/

LogEntry Log[LOG_ENTRY_SIZE] = {{0}};

union LoggingData loggingData;

unsigned short CurrentLogEntry = 0;
unsigned short CurrentDataEntry = 0;


int Logger_init(void){
	return LogText("Logger Started");
}

int LogData(LogType msgType ,LogVar varType, byte NumArgs, ...){
	va_list argp;
	int i = 0;

	LogEntry * NewEntry = &Log[CurrentLogEntry];
	NewEntry->time = X32_us_clock;
	NewEntry->msgType = msgType;
	NewEntry->varType = varType;
	NewEntry->numOfVars = NumArgs;
	NewEntry->dataLoc = CurrentDataEntry;

	va_start(argp, NumArgs);


	if(LOG_ENTRY_SIZE == CurrentLogEntry){
		return OUT_OF_ENTRIES;
	}

	for(i = 0; i < NumArgs; i++){
		if(CurrentDataEntry > LOG_DATA_SIZE - 4){
			va_end(argp);
			return OUT_OF_DATA;
		}

		switch(varType){
		case UINT8:
			loggingData.uint8[CurrentDataEntry] = (uint8_t) va_arg(argp,int);
			CurrentDataEntry++;
			break;
		case UINT16:
			loggingData.uint16[CurrentDataEntry/2] = (uint8_t) va_arg(argp,int);
			CurrentDataEntry += 2;
			break;
		case UINT32:
			loggingData.uint32[CurrentDataEntry/4] = (uint8_t) va_arg(argp,int);
			CurrentDataEntry += 4;
			break;
		case INT8:
			loggingData.int8[CurrentDataEntry] =  (char) va_arg(argp,int);
			CurrentDataEntry++;
			break;
		case INT16:
			loggingData.int16[CurrentDataEntry/2] = (uint16_t) va_arg(argp,int);
			CurrentDataEntry += 2;
			break;
		case INT32:
			loggingData.int32[CurrentDataEntry/4] = (int) va_arg(argp,int);
			CurrentDataEntry += 4;
			break;

		default:
			va_end(argp);
			return (INVAILED_TYPE);
			break;
		}
	}
	if(CurrentDataEntry % 4 != 0){
		CurrentDataEntry+= 4 - (CurrentDataEntry%4);
	}

	CurrentLogEntry++;

	va_end(argp);
	return SUCCESS;
}

int LogText(const char * text){

	LogEntry * NewEntry = &Log[CurrentLogEntry];
	NewEntry->time = X32_us_clock;
	NewEntry->msgType = TEXT;
	NewEntry->varType = CHAR;
	NewEntry->numOfVars = strlen(text);
	NewEntry->dataLoc = CurrentDataEntry;
	memcpy(&loggingData.uint8[CurrentDataEntry],text,NewEntry->numOfVars);

	if(LOG_ENTRY_SIZE == CurrentLogEntry){
		return OUT_OF_ENTRIES;
	}

	if(CurrentDataEntry > LOG_DATA_SIZE - 16){
		return OUT_OF_DATA;
	}

	CurrentDataEntry += NewEntry->numOfVars;

	if(CurrentDataEntry % 4 != 0){ /*ensure DWORD Allingment*/
		CurrentDataEntry += 4 - (CurrentDataEntry%4);
	}

	CurrentLogEntry++;

	return SUCCESS;
}

static unsigned int TypeToSize(LogVar x){
	switch(x){
	case CHAR:
	case UINT8:
	case INT8:
		return 1;

	case UINT16:
	case INT16:
		return 2;

	case UINT32:
	case INT32:
		return 4;
	default:
		return 4;
	}
}

void SendLog(void){
	int i;
	int msgSize = 0;

	LogSendFormat sendBuffer;

	for(i = 0; i < CurrentLogEntry; i++){
		sendBuffer.time = Log[i].time;
		sendBuffer.msgType = Log[i].msgType;
		sendBuffer.varType = Log[i].varType;
		sendBuffer.numOfVars = Log[i].numOfVars;

		/*ohhh, if i see how dirty this is 0.o*/
		memcpy(&sendBuffer.data[0], &loggingData.int8[Log[i].dataLoc], TypeToSize(sendBuffer.varType) * sendBuffer.numOfVars);

		msgSize = sendBuffer.numOfVars * TypeToSize(sendBuffer.varType) + 16;

		/* check if send buffer has space for the message */
		while(uart_outbuffer_space() > BUFFERSIZE/2){
			//protocol_send_new_message(TYPE_LOG, msgSize, (void *)&sendBuffer);
		}

#ifdef __linux_DEBUG__
		PrintLine(&sendBuffer);
	}
	printf("\n\n\n");
#else
	}
#endif
}

#ifdef __linux_DEBUG__ /*for debugging purposes, wont be in the quadro copter*/

static char * TypeToString(LogType type){
	switch(type){
	case TEXT: return "Text";
	case JOYSTICK: return "Joystick";
	case ENGINE: return "Engine";
	case SENSOR: return "Sensor";
	default: return "unknown";
	}
}

void PrintLine(LogSendFormat * log){
	int j;

	printf("%d: %s -\t", log->time, TypeToString(log->msgType));

	switch(log->varType){
	case UINT8:
		for (j = 0; j < log->numOfVars; j++){
			printf("%u, ",(unsigned char) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case UINT16:
		for (j = 0; j < log->numOfVars; j++){
			printf("%u, ",(unsigned short) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case UINT32:
		for (j = 0; j < log->numOfVars; j++){
			printf("%u, ",(unsigned int) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case INT8:
		for (j = 0; j < log->numOfVars; j++){
			printf("%d, ",(char) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case INT16:
		for (j = 0; j < log->numOfVars; j++){
			printf("%d, ",(short) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case INT32:
		for (j = 0; j < log->numOfVars; j++){
			printf("%d, ",(int) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	case CHAR:
		for (j = 0; j < log->numOfVars; j++){
			printf("%c",(char) log->data[j* TypeToSize(log->varType)]);
		}
		break;
	default:
		printf("Unknown type! ERROR!");
		break;
	}
	printf("\n");
}

void PrintLog(void){
	int i = 0;
	int j = 0;

	while (Log[i].time != 0){/*short therm solution*/
		LogEntry * NewEntry = &Log[i];
		printf("%d: %s -\t", NewEntry->time, TypeToString(NewEntry->msgType));

		switch(NewEntry->varType){
		case UINT8:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%u, ",(unsigned char) loggingData.uint8[NewEntry->dataLoc+j]);
			}
			break;
		case UINT16:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%u, ",(unsigned short) loggingData.uint8[NewEntry->dataLoc+j*2]);
			}
			break;
		case UINT32:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%u, ",(unsigned int) loggingData.uint8[NewEntry->dataLoc+j*4]);
			}
			break;
		case INT8:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%d, ",(char) loggingData.int8[NewEntry->dataLoc+j]);
			}
			break;
		case INT16:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%d, ",(short) loggingData.int8[NewEntry->dataLoc+j*2]);
			}
			break;
		case INT32:
			for (j = 0; j < NewEntry->numOfVars; j++){
				printf("%d, ",(int) loggingData.int8[NewEntry->dataLoc+j]*4);
			}
			break;
		case CHAR:
			printf("%s", &loggingData.int8[NewEntry->dataLoc]);
			break;

		}
		printf("\n");
		i++;
	}
}

int main (void){
	InitLogger();
	LogData(JOYSTICK ,UINT16, 7, 1,2,3,4,5,6,7,8,9);
	InitLogger();

	SendLog();

	PrintLog();
	return 0;
}
#endif


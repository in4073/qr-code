#ifndef _LOGGER_H_GUARD_
#define _LOGGER_H_GUARD_

#ifdef __linux_DEBUG__
#include <stdint.h>
#define X32_us_clock 1 /*this is a stub*/

#else
#define X32_us_clock		peripherals[PERIPHERAL_US_CLOCK]
#include <x32.h>
#endif

#define LOG_ENTRY_SIZE 1024 	/* LOG_DATA_SIZE * 10 = bytes of mem used for entry*/
#define LOG_DATA_SIZE 16384		/* size of memory allocated for holding logged data in bytes. 16384 means 16 bytes per entry*/

/*logger error codes*/
#define SUCCESS 0;
#define OUT_OF_ENTRIES 1
#define OUT_OF_DATA 2
#define INVAILED_TYPE 3



typedef enum {TEXT, JOYSTICK, ENGINE, SENSOR} LogType;
typedef enum {UINT8, UINT16, UINT32, INT8, INT16, INT32, CHAR} LogVar;

union LoggingData{
	unsigned char uint8[LOG_DATA_SIZE];
	unsigned short uint16[LOG_DATA_SIZE/2];
	unsigned long uint32[LOG_DATA_SIZE/4];
	char int8[LOG_DATA_SIZE];
	short int16[LOG_DATA_SIZE/2];
	long int32[LOG_DATA_SIZE/4];
};


typedef struct LogEntry{
	unsigned int time;
	LogType msgType;
	LogVar varType;
	unsigned int numOfVars;
	unsigned short dataLoc;
} LogEntry;

typedef struct LogSendFormat{
	unsigned int time;
	LogType msgType;
	LogVar varType;
	unsigned int numOfVars;
	char data[100];
}LogSendFormat;

int Logger_init(void);
int LogData(LogType msgType ,LogVar varType, unsigned char NumArgs, ...);
int LogText(const char * text);
void SendLog(void);

#ifdef __linux_DEBUG__
void PrintLine(LogSendFormat * log); /*needs to be declared here for debugging purposes*/
#endif

#endif

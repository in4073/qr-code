#ifndef _500HzLOGGER_H_GUARD_
#define _500HzLOGGER_H_GUARD_
#include "../main.h"

#define LOG_SIZE 5000

typedef struct Log500Hz{
	unsigned int time;
	int sensor[6];
} Log500Hz;

typedef enum {LOGGING = 0, READY_TO_SEND = 1, SENDING = 2, IDLE = 3} LoggerStatus;
typedef enum {SENSORS = 0, FILTERS = 1} LogSelect;

void RunSensorLogger(void);
bool SensorLoggerRun(void);
LoggerStatus SensorLoggerStatus(void);
void SensorLoggerStart(void);
LoggerStatus SensorLoggerSendLog(void);

void printlogsendstatus(void);

#endif

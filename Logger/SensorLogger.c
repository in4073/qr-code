#include "SensorLogger.h"
#include "../main.h"
#include "x32.h"
#include "../protocol.h"
#include "../uart.h"
#include "../debug.h"
#include "../filter.h"
#include "../fixedpoint.h"

Log500Hz Log[LOG_SIZE];
Log500Hz * LogPtr = &Log[0];
LoggerStatus logStatus = IDLE;
unsigned char loggerReadIndex;
unsigned char dataIndex;
LogSelect logSelect = FILTERS;

/*
 * Author: Hajo Kleingeld
 * Returns logger status
 */
LoggerStatus SensorLoggerStatus(void){
	return logStatus;
}

/*
 * Author: Hajo Kleingeld
 * Function witch stores all values, and logs them into a big array
 * returns false if no new sample was available
 */
bool SensorLoggerRun(void){
	if(logSelect == SENSORS)
	{
		dataIndex = SensorWriteIndex;
	}
	else if(logSelect == FILTERS)
	{
		dataIndex = filterReadIndex[0]; /*index should not matter, filers are synchronous*/
	}

	if(loggerReadIndex != dataIndex){
		if(LogPtr < &Log[LOG_SIZE]){
			LogPtr->time = timestamps[loggerReadIndex];

			if(logSelect == SENSORS){
				LogPtr->sensor[0] = sensors[0][loggerReadIndex];
				LogPtr->sensor[1] = sensors[1][loggerReadIndex];
				LogPtr->sensor[2] = sensors[2][loggerReadIndex];
				LogPtr->sensor[3] = sensors[3][loggerReadIndex];
				LogPtr->sensor[4] = sensors[4][loggerReadIndex];
				LogPtr->sensor[5] = sensors[5][loggerReadIndex];
			}
			else if (logSelect == FILTERS){
				LogPtr->sensor[0] = filterOutput[0][loggerReadIndex];
				LogPtr->sensor[1] = filterOutput[1][loggerReadIndex];
				LogPtr->sensor[2] = filterOutput[2][loggerReadIndex];
				LogPtr->sensor[3] = filterOutput[3][loggerReadIndex];
				LogPtr->sensor[4] = filterOutput[4][loggerReadIndex];
				LogPtr->sensor[5] = filterOutput[5][loggerReadIndex];
			}
			LogPtr++;

			(loggerReadIndex == SENSOR_BUFFER_SIZE -1) ? (loggerReadIndex = 0) : (loggerReadIndex++);
			return true;
		}
		else{
			debug_printf("Done logging");
			logStatus = READY_TO_SEND;
			LogPtr = &Log[0];
		}
	}

	return false
}

/*
 * Author: Hajo Kleingeld
 * Runs the sensor logger, called from the Scheduler
 */
void RunSensorLogger(void){
	if(logStatus == LOGGING){
		while(SensorLoggerRun());
	}
}

/*
 * Author: Hajo Kleingeld
 * Prints the progress of the logger if its sending data to the user
 */
void printlogsendstatus(void){
	if(logStatus == SENDING)
		debug_printf("Sending log, %d lines to go...",&Log[LOG_SIZE] - LogPtr);
}

/*
 * Author: Hajo Kleingeld
 * Starts the logging if called.
 */
void SensorLoggerStart(void){
	logStatus = LOGGING;
	LogPtr = &Log[0];

	if(logSelect == SENSORS)
	{
		loggerReadIndex = SensorWriteIndex;
	}
	else if(logSelect == FILTERS)
	{
		loggerReadIndex = filterReadIndex[0]; /*index should not matter, filers are synchronous*/
	}

	debug_printf("Started logging");
}

/*
 * Author: Hajo Kleingeld
 * Gets called from scheduler if sending the log.
 * Sends the log while there is space in the uart send buffer.
 */
LoggerStatus SensorLoggerSendLog(void){
	int msgSize = sizeof(Log500Hz);

	if(logStatus == IDLE){
		return logStatus;
	}
	else if(logStatus == READY_TO_SEND){
		logStatus = SENDING;
		debug_printf("Start sending log");
	}

	if((LogPtr < &Log[LOG_SIZE]) && (logStatus == SENDING)){
		if(uart_outbuffer_space() > BUFFERSIZE/2)
		{
			/*send log line*/
			protocol_send_new_message(TYPE_LOG_SENSORS, msgSize, (void *)LogPtr);
			//use debug_printf to send debug info using the protocol to the pc program

			LogPtr++;
		}
	}

	/*done sending log*/
	if(LogPtr == &Log[LOG_SIZE]){
		logStatus = IDLE;
		debug_printf("done sending log");
	}
	return logStatus;
}

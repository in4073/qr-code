#ifndef _STATUS_LOGGER_H_
#define _STATUS_LOGGER_H_

#include "SensorLogger.h"

#define LOG_STATUS_SIZE 5000

typedef struct LogStatus {
	unsigned int timestamp;
	uint16_t in_lift;
	int8_t in_roll;
	int8_t in_pitch;
	int8_t in_yaw;
	int8_t mode;
	int8_t ae1;
	int8_t ae2;
	int8_t ae3;
	int8_t ae4;
	uint16_t allignment;	//struct should always have size which is multiple of 4 byte (32 bit)
} LogStatus;

void set_current_inputs(uint16_t l, int8_t r, int8_t p, int8_t y);
LoggerStatus status_logger_state();
void status_logger_start();
void log_status_line();
void print_status_log_send_progress();
LoggerStatus status_logger_send_log();

#endif

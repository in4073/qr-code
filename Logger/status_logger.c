#include "status_logger.h"
#include "../main.h"
#include "x32.h"
#include "../debug.h"
#include "../engines.h"
#include "SensorLogger.h"
#include "../protocol.h"
#include "../statemachine.h"
#include "../EngineControl/EngineControl.h"

LogStatus log[LOG_STATUS_SIZE] = {0};
int log_index = 0;
LoggerStatus l_state = IDLE;

//Raw incoming values from the PC;
uint16_t pc_lift;
int8_t pc_roll;
int8_t pc_pitch;
int8_t pc_yaw;


/*
 * Author: Eric Camellini
 * Returns the state of the status logger.
 */
LoggerStatus status_logger_state(){
  return l_state;
}


/*
 * Author: Eric Camellini
 * If the state is LOGGING, store a log line in the proper memory location.
 */
void log_status_line(){
  if(l_state == LOGGING && log_index < LOG_STATUS_SIZE){
    log[log_index].timestamp = (int) X32_us_clock;
    log[log_index].mode = (int8_t) getMainState();
    log[log_index].in_lift = (uint16_t) lift;
    log[log_index].in_roll = roll;
    log[log_index].in_pitch = pitch;
    log[log_index].in_yaw = yaw;
    log[log_index].ae1 = getEngine1();
    log[log_index].ae2 = getEngine2();
    log[log_index].ae3 = getEngine3();
    log[log_index].ae4 = getEngine4();
    log_index++;
  } else if(l_state == LOGGING) {
    debug_printf("Done status logging.");
    l_state = READY_TO_SEND;
    log_index = 0;
  }

}


/*
 * Author: Eric Camellini
 * Method called when receiving raw controls from the PC, to store them
 * in variables that will be logged if the LOGGING state is active.
 */
void set_current_inputs(uint16_t l, int8_t r, int8_t p, int8_t y){
  pc_lift = l;
  pc_roll = r;
  pc_pitch = p;
  pc_yaw = y;
}


/*
 * Author: Eric Camellini
 * Start logging (Set the status_log in LOGGING state).
 */
void status_logger_start(){
	l_state = LOGGING;
	log_index = 0;
	debug_printf("Started status logging.");
}


/*
 * Author: Eric Camellini
 * Debug function, it sends the log sending progress on the PC.
 */
void print_status_log_send_progress(){
	if(l_state == SENDING)
		debug_printf("Status log, %d lines to go...", LOG_STATUS_SIZE - log_index);
}


/*
 * Author: Eric Camellini
 * Starts sending the log switching to the SENDING state if it is READY_TO_SEND.
 * Sends a log line if the state is SENDING.
 * Switch to IDLE state when there are no more lines to send.
 */
LoggerStatus status_logger_send_log(){
	int msg_size = sizeof(LogStatus);

	if(l_state == READY_TO_SEND){
		l_state = SENDING;
		debug_printf("Start sending status log.");
	}

	if((log_index < LOG_STATUS_SIZE) && (l_state == SENDING)){
		if(uart_outbuffer_space() > BUFFERSIZE/2)
		{
			/*send log line*/
			protocol_send_new_message(TYPE_LOG_TELEMETRY, 14, (void *) &log[log_index]);
			log_index++;
		}
	}

	/*done sending log*/
	if(log_index == LOG_STATUS_SIZE){
		l_state = IDLE;
		debug_printf("Done sending status log.");
	}

  return l_state;
}

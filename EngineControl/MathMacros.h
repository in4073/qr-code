#ifndef _MATHMACROS_H_GUARD_
#define _MATHMACROS_H_GUARD_

/* clamps value bewteen 2 values ~ x = x is a nasty stub to ensure nothing happens.*/
#define CLAMP(x, low, high) (x < (low)) ? (x = (low)):((x > (high)) ? (x = (high)) : (x = x));


#endif

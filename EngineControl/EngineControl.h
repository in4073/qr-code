#ifndef _ENGINECONTROLL_H_GUARD_
#define _ENGINECONTROLL_H_GUARD_

void SetControls(char, char, char, short);
void RunManualControl(void);
void calculateEngineValues(short, short, short, short);
short getLift();
void printControlValues(void);

#define MAX_NEG_DEVIATION 255
#define MAX_POS_DEVIATION 255
#define MIN_ENGINE_VALUE 250
#define MIN_CONTROL_VALUE 150


#undef EXTERN /*nessesary if more .h files with global declarations are in the code*/
#ifdef FILTER_ENGINECONTROL_DEFENITIONS
#define EXTERN
#else
#define EXTERN extern
#endif

EXTERN short Liftoff = 0;

/*telemetry functions*/
short getCurrRoll(void);
short getCurrPitch(void);
short getCurrYaw(void);

extern short yaw, pitch, roll, lift;

#endif

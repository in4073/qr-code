#define FILTER_ENGINECONTROL_DEFENITIONS

#include "../EngineControl/EngineControl.h"
#include "../EngineControl/MathMacros.h"
#include "../debug.h"
#include "../engines.h"
#include "../Logger/status_logger.h"

/**
 * These setpoints are set from the protocol module, and read by both this engine controller and the pid controllers.
 */
short roll = 0; 	/*side angle - min = -45 degrees, max = 45 degrees*/
short pitch = 0; 	/*forward/backward angle - min = -45 degrees, max = 45 degrees*/
short yaw = 0; 		/*twist - min = turn left, max is turn right*/
short lift = 0;

/*
 * Author: Hajo Kleingeld
 * Called from the protocol. Sets all controll values from the user's joystick
 * These values will be used in all of the control functions.
 */
void SetControls(char Roll, char Pitch, char Yaw, short Lift) {
	roll = Roll;
	pitch = Pitch;
	yaw = Yaw;
	lift = Lift;

	CLAMP(lift,0,0x3ff);
}

/*
 * Author: Hajo Kleingeld
 * Code witch runs manual mode, called form scheduler.
 */
void RunManualControl(void){
	calculateEngineValues(roll, pitch, yaw, lift);
}

/**
 * Author: Hajo Kleingeld
 * Calculates the engine values based on give roll, pitch, yaw and lift.
 * Used by both manual control (using direct values from pc) and PID controllers (using output of controller).
 */
void calculateEngineValues(short roll, short pitch, short yaw, short lift) {
	/*ensure no negative value for lift*/
	int ae1 = lift;
	int ae2 = lift;
	int ae3 = lift;
	int ae4 = lift;

	if(lift > MIN_CONTROL_VALUE) {
		ae2 += roll;
		ae4 -= roll;

		ae1 += pitch;
		ae3 -= pitch;

		ae1 -= yaw;
		ae3 -= yaw;
		ae2 += yaw;
		ae4 += yaw;
	}

	if(lift > MIN_ENGINE_VALUE){
		Liftoff = 1;
	}
	else if (lift < MIN_ENGINE_VALUE){
		Liftoff = 0;
	}

	if(Liftoff){
		if(lift > 0x3ff - MAX_POS_DEVIATION){
			CLAMP(ae1,lift-MAX_NEG_DEVIATION,lift+MAX_POS_DEVIATION);
			CLAMP(ae2,lift-MAX_NEG_DEVIATION,lift+MAX_POS_DEVIATION);
			CLAMP(ae3,lift-MAX_NEG_DEVIATION,lift+MAX_POS_DEVIATION);
			CLAMP(ae4,lift-MAX_NEG_DEVIATION,lift+MAX_POS_DEVIATION);
		}
		else{
			CLAMP(ae1,lift-MAX_NEG_DEVIATION,0x3ff);
			CLAMP(ae2,lift-MAX_NEG_DEVIATION,0x3ff);
			CLAMP(ae3,lift-MAX_NEG_DEVIATION,0x3ff);
			CLAMP(ae4,lift-MAX_NEG_DEVIATION,0x3ff);
		}
	}
	else{
		CLAMP(ae1,0,0x3ff);
		CLAMP(ae2,0,0x3ff);
		CLAMP(ae3,0,0x3ff);
		CLAMP(ae4,0,0x3ff);
	}

	SetEngineValues(ae1, ae2, ae3, ae4);
}

/*
 * Author: Hajo Kleingeld
 * Checks if all controlls are 0
 */
short controlsZero() {
	return (roll == 0 && pitch == 0 && yaw == 0 && lift == 0);
}

/*
 * Author: Hajo Kleingeld
 * Prints all controll values
 */
void printControlValues() {
	debug_printf("controls: %d %d %d %d", roll, pitch, yaw, lift);
}

/*
 * Author: Hajo Kleingeld
 * Returns current Roll value
 */
short getCurrRoll(void){
	return roll;
}

/*
 * Author: Hajo Kleingeld
 * Returns current Pitch value
 */
short getCurrPitch(void){
	return pitch;
}

/*
 * Author: Hajo Kleingeld
 * Returns current jaw value
 */
short getCurrYaw(void){
	return yaw;
}

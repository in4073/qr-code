#include "main.h"

#ifndef _STATEMACHINE_H_GUARD_
#define _STATEMACHINE_H_GUARD_

typedef enum {SAFE_MODE, PANIC_MODE, MANUAL_MODE, CALIBRATION_MODE, YAW_CONTROL_MODE, FULL_CONTROL_MODE, SEND_LOG_MODE, EXIT_MODE} ProgramState;

ProgramState getMainState(void);
void PANIC(void);
void RunStateMachine(void);
void setState(uint8_t);


#endif

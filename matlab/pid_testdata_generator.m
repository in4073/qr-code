%vars
data_length = 1000;
SENS_ACC_PITCH = 1;
SENS_ACC_ROLL = 2;
SENS_ACC_YAW = 3;
SENS_GYRO_ROLL = 4;
SENS_GYRO_PITCH = 5;
SENS_GYRO_YAW = 6;

x = 1:data_length;
acc_delay = 6;
zero_sig = zeros(1,data_length);
stepf = smooth([zeros(1,50), 50*ones(1,100), zeros(1,50)],4).';
setpoints = [stepf, stepf, stepf, stepf, zeros(1,data_length-800)]; %50*sin(0.01*x); 

%convert to string and plot
strdata = sprintf('%.0f,', setpoints);
fprintf('short test_setpoint_data[TEST_DATA_L] = {%s};\n', strdata(1:end-1) );

clf;hold on;
colorm=hsv(12);
plot(x,setpoints);

noise_s = awgn(zero_sig, 10, 15);
sensor_g = 15*sin(0.15*x) + setpoints + noise_s; % 
sensor_a = diff( [zeros(1,acc_delay),sensor_g(1:data_length-acc_delay)] );

%convert to string and plot
sensorsstr = {6};
for i = 1: 1 : data_length
    if i == SENS_ACC_YAW
        sensorsstr{i} = sprintf('%.0f,', sensor_a);
    elseif i == SENS_GYRO_YAW
        sensorsstr{i} = sprintf('%.0f,', sensor_g);
    else
        % default: noise
        sensorsstr{i} = sprintf('%.0f,', awgn(zero_sig, 5, 10));
    end;
end
fprintf('short test_filter_data[6][TEST_DATA_L] = {\n\t{%s},\n\t{%s},\n\t{%s},\n\t{%s},\n\t{%s},\n\t{%s}\n};\n', sensorsstr{1}(1:end-1), sensorsstr{2}(1:end-1), sensorsstr{3}(1:end-1), sensorsstr{4}(1:end-1), sensorsstr{5}(1:end-1), sensorsstr{6}(1:end-1));
plot(x,sensor_g,x,[sensor_a 1]);

csv = csvread('../../pc/logs/log_sensors.csv');
scrsz = get(groot,'ScreenSize'); % [left bottom width height]

%plot results also in same figure
plot(x(1:length(csv(:,2))), csv(:,2));
plot(x(1:length(csv(:,3))), csv(:,3));
plot(x(1:length(csv(:,4))), csv(:,4));
plot(x(1:length(csv(:,5))), csv(:,5));
plot(x(1:length(csv(:,6))), csv(:,6), 'color', colorm(2,:));
plot(x(1:length(csv(:,7))), csv(:,7)./2^12, 'color', colorm(7,:)); 
legend();

legend('Setpoints','Sensor gyro','Sensor acc', 'Engine 1', 'Engine 2', 'Engine 3', 'Engine 4', 'Calculated yaw/pitch/roll', 'yaw/pitch/roll filter output');
close all

%choose file by changing index
file_index = 9;
path = ''; % '../../pc/logs/';
%cd(path);
csv_files = dir(strcat(path, '*.csv'));
csv_file = csv_files(file_index).name;

for i = 1: numel(csv_files)
    pathstr  = what(path);
    fprintf('%.0f: %s\\%s\n', i, pathstr.path, csv_files(i).name);
end;

csv = csvread(csv_file);
scrsz = get(groot,'ScreenSize'); % [left bottom width height]



% calibration (quick and dirty): get avg of all samples and substract from all samples
for i = 1 : 1 : 7
    csv(:,i) = csv(:,i) ./ 2^12;
    %avg = mean(csv(1:100,i));
    %csv(:,i) = csv(:,i) - avg;
end;


figure('Name',strcat('Accelerometers (', csv_file ,')'),'Position',[0.02*scrsz(3) 0.2*scrsz(4) 0.45*scrsz(3) 0.6*scrsz(4)], 'NumberTitle', 'off');
hold on;
plot(csv(:,1), csv(:,2));
plot(csv(:,1), csv(:,3));
plot(csv(:,1), csv(:,4));
legend('Acc 1', 'Acc 2', 'Acc 3');

figure('Name',strcat('Gyroscopes (', csv_file ,')'),'Position',[0.5*scrsz(3) 0.2*scrsz(4) 0.45*scrsz(3) 0.6*scrsz(4)], 'NumberTitle', 'off');
hold on;
plot(csv(:,1), csv(:,5));
plot(csv(:,1), csv(:,6));
plot(csv(:,1), csv(:,7));
legend('Gyro 1', 'Gyro 2', 'Gyro 3');

%cd('../../fpga/matlab');
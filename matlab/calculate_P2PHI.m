csv_pitch_1 = csvread('log_rate_90_degrees_pitch.csv');
csv_roll_1 = csvread('log_rate_90_degrees_roll.csv');

%choose file by changing index
file_index = 1;
csv_files = dir('*.csv');
csv_file = csv_files(file_index).name;
csv = csvread(csv_file);
scrsz = get(groot,'ScreenSize'); % [left bottom width height]

% calibration (quick and dirty): get avg of all samples and substract from all samples
for j = 1 : 1 : 7
    avg = mean(csv(:,j));
    csv(:,j) = csv(:,j) - avg;
end;

% calculate integration of signal between two zero crossings
if file_index == 1
    gsensor = 6;
    asensor = 4;
else
    gsensor = 5;
    asensor = 3;
end;

integr = 1:2;
time = 1:2;
freq = 500;
prevv = 0;
startv = 1;
k = 1;
csvlength = length(csv(:,gsensor));

for j = 1 : 1 : csvlength
    currentv = csv(j,gsensor);
    % if signal crosses zero
    if (currentv > 0 && prevv <= 0) || (currentv < 0 && prevv >= 0)
        sumv = sum(csv(startv:j,gsensor));

        %fprintf('Zero point %f\n', j);

        if abs(sumv) > 500
            integr(k) = abs(sumv);
            %fprintf('Integral (%.0f-%.0f) %f\n', startv, j, integr(k));
            time(k) = (j - startv); %time (s) for 90 degr movement
            k = k + 1;
        end;

        startv = j;
    end;
    prevv = currentv;
end;

% So the factor (P2PHI) is maximum gyro value/maximum acc value * area of
% one 90 deg movement / number of steps duren that movement
factor = (30/128)*mean(integr)/(mean(time)*freq);
fprintf('Average area for %.0f = %f, factor %f\n', gsensor, mean(integr), factor);

rate = zeros(1,csvlength);
for j = 2 : 1 :  csvlength
    rate(j) = rate(j-1) + factor * csv(j,gsensor);
end;

clf;hold on;
plot(csv(:,1), csv(:,asensor));
plot(csv(:,1), csv(:,gsensor));
plot(csv(:,1), rate);
legend('Accelerometer', 'Gyrosensor', 'Angle');
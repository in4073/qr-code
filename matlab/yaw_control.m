% Constants
vect_l = 1000;   % number of steps
noise = 40;     % noise in dB
Kp = 0.55;      % proportion parameter
Ki = 0.5;
Kd = 0.02;      % derivative parameter
delay = 1;      % in s
dt = 0.1;     % in s

% Create vector of length vect_l with 25dB noise
zero_sig = zeros(1,vect_l);
noise_s = awgn(zero_sig, noise);
s_yawrate = noise_s;
N_s = zeros(1,vect_l);
x = 1:vect_l;
e = zeros(1,vect_l);

% rate change
sine_edge = (1 + sin( pi/(125) * (0:124) - (pi/2)) )*0.5;
yaw_setp = [zeros(1,125),sine_edge,ones(1,vect_l-250)];
I = 0;

% caculate engine setpoint
for i = 2: 1: length(s_yawrate)
    % calculate derivative using step size
    e(i) = yaw_setp(i) - s_yawrate(i);
    I = I + e(i)*dt;
    D = ( e(i) - e(i-1) )/dt;
    N_s(i) = Kp*e(i) + Ki*I + Kd*D;
    
    % estimate next yawrate sensor value with sensor noise
    s_yawrate(i+1) = N_s(i) + noise_s(i);
end;

% create plot
clf;
hold on;

h = plot(x, s_yawrate(1:vect_l));
h = plot(x, N_s);
h = plot(x, yaw_setp);
h = plot(x, e);
set(h(1), 'LineWidth', 1);
h.Color(4) = 0.2;
%set(h(1),'linewidth',5);
legend('Sensor','Engine','JS input', 'Error');
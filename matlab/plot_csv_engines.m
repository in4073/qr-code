close all

%choose file by changing index
file_index = 11;
csv_files = dir('*.csv');
csv_file = csv_files(file_index).name;
csv = csvread(csv_file);
scrsz = get(groot,'ScreenSize'); % [left bottom width height]

figure('Name',csv_file,'Position',[0.02*scrsz(3) 0.2*scrsz(4) 0.45*scrsz(3) 0.6*scrsz(4)], 'NumberTitle', 'off');
hold on;
plot(csv(:,1), csv(:,2));
plot(csv(:,1), csv(:,3));
plot(csv(:,1), csv(:,4));
plot(csv(:,1), csv(:,5));
legend('Engine 1', 'Engine 2', 'Engine 3', 'Engine 4');
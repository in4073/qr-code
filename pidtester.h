#ifndef PIDTESTER_H
#define PIDTESTER_H

	//if TEST_MODE is defined, fake sensor data is used
	//#define TEST_MODE
	#define TEST_DATA_L 	1000

	#define TEST_MODE_DIV	25

	//RAND_MAX has to be defined for the rand() function
	#define RAND_MAX		20

	#ifdef TEST_MODE
		extern short test_setpoint_data[TEST_DATA_L];
		extern short test_filter_data[6][TEST_DATA_L];
	#else
		extern short test_setpoint_data[1];
		extern short test_filter_data[6][1];
	#endif

	extern int test_read_index;

	int pidtest_get_sensor(int);
	void pidtest_set_next_setpoint();
	void pidtest_send_log();

#endif

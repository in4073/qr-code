#ifndef PROTOCOL_H
#define PROTOCOL_H

	#define START_BYTE_VALUE	0x4D
	#define MIN_MESSAGE_SIZE	3
	#define START_BYTE			0
	#define MESSAGE_TYPE_BYTE	1
	#define PAYLOAD_BYTE		2

	#define TYPE_CONTROL		1
	#define TYPE_MODE			2
	#define TYPE_ALGO_PARAMS	3
	#define TYPE_DEBUG			4
	#define TYPE_TELEMTERY		5
	#define TYPE_ACK			6
	#define TYPE_LOG_START		7
	#define TYPE_LOG_REQUEST	8
	#define TYPE_LOG_SENSORS	9
	#define TYPE_LOG_TELEMETRY  10

	//define the size of the message types

	extern byte MessageTypeSize[];
	extern bool pc_connected;

	typedef struct message {
		byte startbyte;
		byte type;
		byte checksum;
		byte* payload;
	} Message;

	void RunProtocol(void);
	bool protocol_check_message(void);
	bool protocol_parse_message(void);
	void protocol_send_fixed_message(byte, int, ...);
	Message* protocol_create_message(byte, int, void*);
	void protocol_send_message(Message*, int);
	void protocol_send_new_message(byte, int, void*);
	void protocol_ack(char);
	bool protocol_buffer_empty(void);
	int protocol_message_size(int);

#endif

extern byte fifo[];

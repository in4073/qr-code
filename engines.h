#include "main.h"

#ifndef _ENGINES_H_GUARD
#define _ENGINES_H_GUARD

#define MAX_ENGINE_POWER 0x3FF
#define ENGINE_ADJUSTMENT 2
#define SENS_DATA_SRC	SENSORS //TEST_DATA

bool RunPanic(void);
void _updateEngineValues(void);
void Engines_init(void);
void SetEngineValues (unsigned short e1, unsigned short e2, unsigned short e3, unsigned short e4);
void EnginePANIC(void);
bool EnginesOff(void);
void printEngineValues(void);

typedef struct Engine{
	unsigned short EngineSetpoint;
	unsigned short real;
}Engine;

unsigned short getEngine1(void);
unsigned short getEngine2(void);
unsigned short getEngine3(void);
unsigned short getEngine4(void);

#endif

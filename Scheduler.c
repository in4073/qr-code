#define SCHEDULER_GLOBAL_DEFENITIONS
#include "Scheduler.h"
#include "x32.h"
#include "main.h"
#include "LazyTimer/LazyTimer.h"
#include "Logger/status_logger.h"
#include "pidcontroller.h"
#include "debug.h"
#include "filter.h"
#include "statemachine.h"
#include "calibration.h"
#include "protocol.h"
#include "EngineControl/EngineControl.h"
#include "Logger/SensorLogger.h"
#include "engines.h"
#include "string.h"


typedef struct functionTime{
	unsigned int nrOfExecutions;
	unsigned int executionTime;
	unsigned int totalExecutionTime;
}functionTime;

functionTime taskStatistics[30] = {0};
LazyTimer oneSecondTimer, tenSecondTimer, telemetry_timer, loopTimer;

static int s_time = 0;

#define START_MEASURE s_time = X32_us_clock
#define STOP_MEASURE 	taskStatistics[*currentScedule].executionTime = X32_us_clock - s_time;\
						taskStatistics[*currentScedule].totalExecutionTime += taskStatistics[*currentScedule].executionTime;\
						taskStatistics[*currentScedule].nrOfExecutions++;\

#define SetNextTask()	currentScedule++

unsigned int thisLoopStartTime = 0;
unsigned int ttp = 0;
unsigned int ttg = 0;


static Tasks * currentScheduleStart;
static Tasks * currentScedule;

unsigned int SlowTime = 0;
unsigned int MaxLoopTime = 0;
unsigned int TotalLoopTime = 0;
unsigned int NrOfExecutions = 0;

/**
 * Author: Hajo Kleingeld
 * Resets all timing statistics. mostly called when switching modes.
 */
void resetTimingStatistics(void){
	memset(&taskStatistics[0], 0, sizeof(taskStatistics));
	TotalLoopTime = 0;
	MaxLoopTime = 0;
	SlowTime = 0;
	NrOfExecutions = 0;
}

/**
 * Author: Hajo Kleingeld
 * Sends telemetry data.
 */
void sendTelemetry(void){
	telemetryData telMsg;

	if(uart_outbuffer_space() > 30){
		telMsg.avgLoopTime = TotalLoopTime/NrOfExecutions;
		telMsg.maxLoopTime = MaxLoopTime;
		telMsg.missedDeadlines = SlowTime;
		telMsg.currActualRoll = getCurrRoll();
		telMsg.currActualPich = getCurrPitch();
		telMsg.currActualYaw = getCurrYaw();
		telMsg.engine1 = getEngine1();
		telMsg.engine2 = getEngine2();
		telMsg.engine3 = getEngine3();
		telMsg.engine4 = getEngine4();
		telMsg.MainState = getMainState();

		protocol_send_new_message(TYPE_TELEMTERY, sizeof(telemetryData),&telMsg);
	}
}

/**
 * Author: Hajo Kleingeld
 * Gets called form the sceduler, if not lacking.
 * Sends telemetry and other debug stuff.
 */
void debugging_stuff(void){

	if(runTimer(&telemetry_timer,250)){
		ResetTimer(&telemetry_timer);
		sendTelemetry();
		return; /*helps to spread CPU load if more stuff is printed*/
	}

	if(runTimer(&oneSecondTimer, 1000)) {
		printlogsendstatus();
		print_status_log_send_progress();
		led_toggle(LED_1S_RATE);
		ResetTimer(&oneSecondTimer);
		return; /*helps to spread CPU load if more stuff is printed*/
	}

//	if(runTimer(&tenSecondTimer, 10000)){
//		ResetTimer(&tenSecondTimer);
//		debug_printf("filter: %d x %d", taskStatistics[FILTERING].nrOfExecutions, taskStatistics[FILTERING].totalExecutionTime/taskStatistics[FILTERING].nrOfExecutions);
//		debug_printf("protocol: %d x %d", taSlowTimeskStatistics[PROTOCOL].nrOfExecutions, taskStatistics[PROTOCOL].totalExecutionTime/taskStatistics[PROTOCOL].nrOfExecutions);
//		debug_printf("sleep: %d x %d", taskStatistics[SLEEP].nrOfExecutions, taskStatistics[SLEEP].totalExecutionTime/taskStatistics[SLEEP].nrOfExecutions);
//		debug_printf("sleep: %d x %d", taskStatistics[YAW_CONTROL].nrOfExecutions, taskStatistics[YAW_CONTROL].totalExecutionTime/taskStatistics[YAW_CONTROL].nrOfExecutions);
//		printEngineValues();
//		debug_printf(" ");
//	}
}

/**
 * Author: Hajo Kleingeld
 * Sets a new schedule for next itteration of the scheduler.
 */
void setSchedule(Tasks * newSchedule){
	currentScedule = newSchedule;
	currentScheduleStart = newSchedule;
}

/**
 * Author: Hajo Kleingeld
 * Code of the scheduler, pretty complicated, ask it's desinger or read the documentation (to be deliverd @28-10-2015)
 * Basically calls all other code (exept for interrupts) while the QR is opperational based of a schedule defined in scheduler.h
 */
void run(void){
	int newLoopTime = 0;
	setSchedule(&SafeMode[0]);

	initTimer(&oneSecondTimer);
	initTimer(&telemetry_timer);
	initTimer(&loopTimer);

	while(!KickMain); /*wait for synchronization*/
	KickMain = false;

	while(1){
		switch(*currentScedule){
		case FILTERING:
			START_MEASURE;
			RunFilters();
			STOP_MEASURE;

			SetNextTask();
			break;
		case YAW_CONTROL:
			START_MEASURE;
			pid_yaw_control();
			STOP_MEASURE;
			SetNextTask();
			break;

		case FULL_CONTROL:
			START_MEASURE;
			pid_full_control();
			STOP_MEASURE;
			SetNextTask();
			break;

		case PITCH_CONTROL:

			SetNextTask();
			break;
		case ROLL_CONTROL:

			SetNextTask();
			break;
		case ENGINE_CONTROL:
			RunManualControl();
			SetNextTask();
			break;
		case PROTOCOL:
			START_MEASURE;
			RunProtocol();
			STOP_MEASURE;

			SetNextTask();
			break;
		case SENSOR_LOGGER:
			RunSensorLogger();
			log_status_line();
			SetNextTask();
			break;
		case SEND_LOG:
			//send first Sensor logger, then status logger
			if(SensorLoggerSendLog() == IDLE){
				if(status_logger_send_log() == IDLE) {
					setState(SAFE_MODE);
				}
			}
			SetNextTask();
			break;
		case CALIBRATION:
			RunCalibration();
			setState(SAFE_MODE);
			print_offsets();
			SetNextTask();
			break;
		case PANIC_:
			if(RunPanic()){
				return; /*reset the scheduler*/
			}
			SetNextTask();
			break;
		case FINISHED:
			if(KickMain){ /*we where too slow*/
				if(getFilterLack() != 1){ /*we are more than 1 ms behind*/
					skipSampleFilter();
					SlowTime++;
				}
			}
			else{
				uart_start_snd();
			}
			/*do timing calculations*/

			SetNextTask();
			newLoopTime = TimePassed_us(&loopTimer);
			TotalLoopTime += newLoopTime;
			NrOfExecutions++;
			if(newLoopTime > MaxLoopTime)
				MaxLoopTime = newLoopTime;
			break;
		case SLEEP:
		default:
			RunStateMachine();
			if(KickMain){
				KickMain = false;
				ResetTimer(&loopTimer);
				runTimer_us(&loopTimer,10000);
				setSchedule(currentScheduleStart);
			}

			debugging_stuff();
			break;
		}
	}

}

/**
 * Author: Hajo Kleingeld
 * Init function of the scheudler. It Syncs the filters with the incoming sensorvalues, then starts the scheduler.
 */
void startProgram(void){
	while(1){
		/*run all filters until we are up to date*/
		setState(SAFE_MODE);
		runFiltersFull();
		KickMain = false;

		run();
	}
}

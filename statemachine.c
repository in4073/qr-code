#include <stdlib.h>
#include "statemachine.h"
#include "EngineControl/EngineControl.h"
#include "engines.h"
#include "debug.h"
#include "LazyTimer/LazyTimer.h"
#include "Logger/SensorLogger.h"
#include "Logger/status_logger.h"
#include "protocol.h"
#include "filter.h"
#include "pidcontroller.h"
#include "calibration.h"
#include "pidtester.h"
#include "main.h"
#include "Scheduler.h"

/**
 * Init global variables
 */
static ProgramState MainState = SAFE_MODE;
ProgramState nextState = 0;

LoggerStatus s1;
LoggerStatus s2;

/*
 * Author: Hajo Kleingeld
 * returns the state of the QR
 */
ProgramState getMainState(void){
	return MainState;
}

/*
 * Author: Eric Camellini
 * Function to exit the program and returning to the fpga boot_loader.
 */
void end_program(){
	debug_printf("QR program terminated.");
	led_all_off(0xFF);
	exit(0);
}

/*
 * Author: Hajo Kleingeld
 * Function witch can be called to switch to panic mode.
 */
void PANIC(void){
	setState(PANIC_MODE);
}

/*
 * Author: Corniël Joosse
 * setState is called from e.g. the protocol module to set the state machine to a certain mode.
 * For some mode checks have to be done before change is valid.
 */
void setState(uint8_t state) {
	nextState = state;
}

/*
 * Author: Corniël Joosse
 * Checks whether the state is allowed to change to nextState. nextState is set by setState().
 */
void RunStateMachine(void){
	ProgramState currentState = MainState;
	ProgramState currentNextState = nextState;

	//save compute power if state does not have to be changed
	if(nextState == MainState) {
		return;
	}

	switch(nextState) {
		case SAFE_MODE:
			// safe state can only be entered if previous mode was panic or engine values are 0?
			if(EnginesOff() && pc_connected) {
				MainState = SAFE_MODE;
				setSchedule(SafeMode);
				resetTimingStatistics();
			}
			break;

		case PANIC_MODE:
			// panic mode can always be entered, except when we're in safe mode (then we are already safe)
			MainState = PANIC_MODE;
			/*start panic*/
			EnginePANIC();
			setSchedule(PanicMode);
			break;

		case MANUAL_MODE:
			if((MainState == SAFE_MODE) && controlsZero()) {
				MainState = MANUAL_MODE;
				setSchedule(ManualMode);
				resetTimingStatistics();
			} else {
				debug_printf("MANUAL_MODE - Controls not 0!");
			}
			break;

		case CALIBRATION_MODE:
			if((MainState == SAFE_MODE) && controlsZero()) {
				MainState = CALIBRATION_MODE;
				setSchedule(CalibrationMode);
			}
			break;

		case YAW_CONTROL_MODE:
			if((MainState == SAFE_MODE) && is_calibrated() && controlsZero()) {
				setSchedule(YawControlMode);
				MainState = YAW_CONTROL_MODE;

				resetTimingStatistics();
				#ifdef TEST_MODE
					debug_printf("TEST_MODE enabled");
				#endif
			} else {
				debug_printf("Not calibrated or controls are not zero!");
			}
			break;

		case FULL_CONTROL_MODE:
			if((MainState == SAFE_MODE) && is_calibrated() && controlsZero()) {
				setSchedule(FullControlMode);
				MainState = FULL_CONTROL_MODE;
				#ifdef TEST_MODE
					debug_printf("TEST_MODE enabled");
				#endif
			} else {
				debug_printf("Not calibrated or controls are not zero!");
			}
			break;

		case SEND_LOG_MODE:
			//logs can only be send in safe mode, after sending logs state returns automatically to safe
			if(MainState == SAFE_MODE) {
				if(SensorLoggerStatus() == READY_TO_SEND && status_logger_state() == READY_TO_SEND){
					MainState = SEND_LOG_MODE;
					setSchedule(SendLogMode);
				}
			}
			break;

		case EXIT_MODE:
			if(MainState == SAFE_MODE){
				MainState = EXIT_MODE;
				end_program();
			}
			break;

		default:
			break;
	}

	if(MainState != currentState) {
		//debug that mode has changed
		debug_printf("%d->%d", currentState, MainState);
	}

	//reset next state if not set in this function
	//choice for new state should not be remembered if requirements were not reached
	if(currentNextState == nextState) {
		nextState = MainState;
	}
}

#include <x32.h>
#include <stdio.h>
#include "uart.h"
#include "main.h"
#include "debug.h"

byte snd_byte_cnt = 0;
byte fifo[BUFFERSIZE] = {0};
int	iptr = 0, optr = 0;

byte sndbuf[BUFFERSIZE] = {0};
int	isndptr = 0, osndptr = 0;
bool sndready = 0;

/*
 * Author: Corniël Joosse
 * Init the uart for communication with the pc
 */
void uart_init(void) {
	//initialise variables
	char c;
	iptr = 0;
	optr = 0;
	isndptr = 0;
	osndptr = 0;
	sndready = true;

	// prepare rs232 rx interrupt and getchar handler
	SET_INTERRUPT_VECTOR(INTERRUPT_PRIMARY_RX, &isr_rs232_rx);
	SET_INTERRUPT_PRIORITY(INTERRUPT_PRIMARY_RX, 20);
	while (X32_rs232_char) c = X32_rs232_data; // empty buffer
	ENABLE_INTERRUPT(INTERRUPT_PRIMARY_RX);

	// prepare wireless rx interrupt and getchar handler
	/*SET_INTERRUPT_VECTOR(INTERRUPT_WIRELESS_RX, &isr_wireless_rx);
	SET_INTERRUPT_PRIORITY(INTERRUPT_WIRELESS_RX, 19);
	while (X32_wireless_char) c = X32_wireless_data; // empty buffer
	ENABLE_INTERRUPT(INTERRUPT_WIRELESS_RX);*/

	// prepare rs232 tx interrupt
	SET_INTERRUPT_VECTOR(INTERRUPT_PRIMARY_TX, &isr_rs232_tx);
	SET_INTERRUPT_PRIORITY(INTERRUPT_PRIMARY_TX, 15);
	ENABLE_INTERRUPT(INTERRUPT_PRIMARY_TX);

	// prepare xufo interrupt for led blinking
	SET_INTERRUPT_VECTOR(INTERRUPT_XUFO, &isr_xufo);
	SET_INTERRUPT_PRIORITY(INTERRUPT_XUFO, 21);
	ENABLE_INTERRUPT(INTERRUPT_XUFO);
}

/*
 * Author: Corniël Joosse
 * rs232 rx interrupt handler, fires when byte is received
 */
void isr_rs232_rx(void) {
	int	c;

	// may have received > 1 char before IRQ is serviced so loop
	while (X32_rs232_char) {
		#if DEBUG_MODE == DEBUG_MODE_DIRECT
			fifo[iptr++] = getchar() - '0';
			printf("%x", fifo[iptr-1]);
		#else
			fifo[iptr++] = getchar();
		#endif
		if (iptr >= BUFFERSIZE)
			iptr = 0;
	}
}

/*
 * Author: Corniël Joosse
 * wireless rx interrupt handler, fires when byte is received
 */
/*void isr_wireless_rx(void) {
	int	c;

	// may have received > 1 char before IRQ is serviced so loop
	while (X32_wireless_char) {
		#if DEBUG_MODE == DEBUG_MODE_DIRECT
			fifo[iptr++] = X32_wireless_data - '0';
			printf("%x", fifo[iptr-1]);
		#else
			fifo[iptr++] = X32_wireless_data; //maybe getchar() works also
		#endif
		if (iptr >= BUFFERSIZE)
			iptr = 0;
	}
}*/

/*
 * Author: Corniël Joosse
 * rs232 tx interrupt handler, fires when transmit is done
 */
void isr_rs232_tx(void) {
	uart_putchar();
}

/*
 * Author: Corniël Joosse
 * isr_xufo interrupt handler, fires when peripherals are communicating
 */
void isr_xufo(void) {
	led_toggle(LED_QR_COMM);
}

/*
 * Author: Corniël Joosse
 * Calculates the amount of bytes in the receive buffer
 */
int uart_buffer_size(void) {
	if(iptr >= optr) {
		return iptr-optr;
	} else {
		return BUFFERSIZE - optr + iptr;
	}
}

/*
 * Author: Corniël Joosse
 * Calculates the amount of bytes in the send buffer
 */
int uart_outbuffer_size(void) {
	if(isndptr >= osndptr) {
		return isndptr-osndptr;
	} else {
		return BUFFERSIZE - osndptr + isndptr;
	}
}

/*
 * Author: Corniël Joosse
 * Calculates the amount of space in the send buffer
 */
int uart_outbuffer_space(void) {
	return BUFFERSIZE - uart_outbuffer_size();
}

/*
 * Author: Corniël Joosse
 * Returns the char from the buffer with a certain offset
 */
byte uart_get_byte(int offset) {
	if(uart_buffer_size() > offset) {
		return fifo[(optr+offset)%BUFFERSIZE];
	}
	else {
		return 0;
	}
}

/*
 * Author: Corniël Joosse
 * Discards the first number of bytes from the buffer depending on the given offset.
 */
bool uart_delete_byte(int offset) {
	if(uart_buffer_size() >= offset) {
		optr = (optr+offset)%BUFFERSIZE;
		return true;
	}
	else {
		return false;
	}
}

/*
 * Author: Corniël Joosse
 * Prints the buffer content for debugging purposes
 */
void uart_print() {
	int i = 0, j = uart_buffer_size();

	debug_printf("Buffer: ");
	for(; i<j; i++) {
		debug_printf("%2x ", fifo[(optr+i)%BUFFERSIZE]);
	}
}

/*
 * Author: Corniël Joosse
 * Prints the content of the send buffer, not very useful because it uses also the sendbuffer to send the debug info
 */
void uart_print_outbuffer() {
	int i,j = uart_outbuffer_size();

	debug_printf("Buffer (%d:%d):", osndptr, isndptr);

	for(i = 0; i < j; i++) {
		debug_printf("%02x ", sndbuf[(osndptr+i)%BUFFERSIZE]);
	}
}

/*
 * Author: Corniël Joosse
 * Calculates the checksum of 'nbytes' of the receive buffer starting with 'start'
 */
int uart_checksum(int start, int nbytes) {
	byte sum = 0, i;

	if(uart_buffer_size() >= start + nbytes) {
		for(i = start; i <= nbytes;i++) {
			sum += fifo[(optr+i)%BUFFERSIZE];
		}
	}
	return sum;
}

/*
 * Author: Corniël Joosse
 * Add byte to the send buffer, and if not sending a message right now, start sending
 */
void uart_send_byte(byte b) {
	sndbuf[isndptr] = b;
	isndptr = (isndptr + 1) % BUFFERSIZE;
	#if DEBUG_MODE == DEBUG_MODE_DIRECT
		debug_printf("%02x ", b);
	#else
		if(sndready) uart_putchar();
	#endif

}

/*
 * Author: Corniël Joosse
 * Copies a byte from the send buffer to the peripheral that sends it over the uart.
 * To be able to control the amount of time this function uses, it only sends SND_SEQUENTLY_BYTES bytes.
 * uart_start_snd() triggers again that SND_SEQUENTLY_BYTES are send.
 */
void uart_putchar() {
	#if DEBUG_MODE == DEBUG_MODE_PROTOCOL
		if(snd_byte_cnt++ < SND_SEQUENTLY_BYTES) {
			sndready = false;
			if(isndptr != osndptr && X32_rs232_write) {
				DISABLE_INTERRUPT(INTERRUPT_PRIMARY_TX);
				putchar(sndbuf[osndptr]);
				osndptr = (osndptr + 1) % BUFFERSIZE;
				ENABLE_INTERRUPT(INTERRUPT_PRIMARY_TX);
			} else {
				sndready = true;
			}
		}
	#endif
}

/*
 * Author: Corniël Joosse
 * Triggers the sending of another SND_SEQUENTLY_BYTES bytes by resetting the send counter.
 */
void uart_start_snd() {
	snd_byte_cnt = 0;
	uart_putchar();
}

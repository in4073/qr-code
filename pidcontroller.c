/*
 * pidcontroller.c contains the functions for yaw control and roll/pitch control.
 * To be able to change all parameters, these are defined as global variables.
 */

#include "pidcontroller.h"
#include "debug.h"
#include "protocol.h"
#include "main.h"
#include "fixedpoint.h"
#include "filter.h"
#include "calibration.h"
#include "EngineControl/EngineControl.h"
#include "pidtester.h"

/*
 * Author: Corniël Joosse
 * The corrected yaw, pitch, roll and lift calculated by the controllers
 */
short cyaw, cpitch, croll, clift;

/*
 * Author: Corniël Joosse
 * Controller parameters: (other names than in assignment)
 *   - Py: Proportion (P) for yaw control
 *   - P1pr: Proportion (P) for pitch/roll control
 *   - P2pr: Derivative (D) for pitch/roll control
 *   - C1pr: kalman constant 1
 *   - C2pr: kalman constant 2
 */
FixedPoint Py = {fp_from_int(0.5, PID_PREC), PID_PREC},
		P1pr = {fp_from_int(0.5, PID_PREC), PID_PREC},
		P2pr = {fp_from_int(0.5, PID_PREC), PID_PREC};
int C1pr = 100;
int C2pr = 10000;

int test_mode_cnt = 0;

/*
 * Author: Corniël Joosse
 * Sets the pid controller parameters
 */
void pid_set_params(int Py_n, int P1pr_n, int P2pr_n, int C1pr_n, int C2pr_n) {
	Py = fp(Py_n, PID_PREC);
	fp_div_fc(&Py, &Py, 100);

	P1pr = fp(P1pr_n, PID_PREC);
	fp_div_fc(&P1pr, &P1pr, 100);

	P2pr = fp(P2pr_n, PID_PREC);
	fp_div_fc(&P2pr, &P2pr, 100);

	C1pr = C1pr_n;
	C2pr = C2pr_n;
}

/*
 * Author: Corniël Joosse
 * Calculates corrected yaw value and updates engine values.
 * When TEST_MODE is defined, sensor values and yaw setpoints are provided by pidtester.c.
 */
void pid_yaw_control() {
	#ifdef TEST_MODE
		//test at slow rate so we can also send log messages
		if(test_mode_cnt++ >= TEST_MODE_DIV) {
			test_mode_cnt = 0;

			//update test data and calculate yaw
			pidtest_set_next_setpoint();
			_pid_yaw_controller();
			pidtest_send_log();
		}
	#else
		_pid_yaw_controller();
	#endif
	calculateEngineValues(roll, pitch, cyaw, lift);
}


/*
 * Author: Corniël Joosse
 * Calculates corrected values for yaw, pitch and roll.
 * When TEST_MODE is defined, sensor values and yaw, pitch or roll setpoints are provided by pidtester.c.
 */
void pid_full_control() {
	#ifdef TEST_MODE
		//test at slow rate so we can also send log messages
		if(test_mode_cnt++ > TEST_MODE_DIV) {
			test_mode_cnt = 0;

			//update test data
			pidtest_set_next_setpoint();

			//calculate yaw, pitch and roll
			_pid_yaw_controller();
			_pid_pitch_controller();
			_pid_roll_controller();

			pidtest_send_log();
		}
	#else
		//calculate yaw, pitch and roll
		_pid_yaw_controller();
		_pid_pitch_controller();
		_pid_roll_controller();
	#endif
	//and update engine values
	calculateEngineValues(croll, cpitch, cyaw, lift);

}

/*
 * Author: Corniël Joosse
 * Calculates the corrected yaw by using a P controller and one of the gyroscope sensors.
 */
static void _pid_yaw_controller() {
	int error = 0, new_yaw = 0;
	//char buf[15];

	//P controller without FixedPoint vars, new_yaw = Py*error
	#if DATA_SRC == FILTER_DATA
		error = fp_from_int(yaw, PID_PREC) - FILTER_VAL(SENS_GYRO_YAW, PID_GYRO_SCALAR);
	#else
		error = fp_from_int((yaw - FILTER_VAL(SENS_GYRO_YAW, PID_GYRO_SCALAR)), PID_PREC);
	#endif
	new_yaw = fp_mul_fi(Py.n, error, PID_PREC);

	//fp_print(&new_yaw, buf);
	//debug_printf("y %d ny %d", yaw, fp_int_i(new_yaw, PID_PREC));

	//calculate and save corrected engine value
	cyaw = fp_int_i(new_yaw, PID_PREC);
}

/*
 * Author: Corniël Joosse
 * This function is the implementation of a cascaded P-controller with a Kalman filter.
 */
static void _pid_pitch_controller() {
	/*
	 * init variables, they contain the int part of a fixed point number with precision PID_DEFAULT_PRECISION
	 *   pitch: the pitch attitude setpoint
	 * 	 pitch_r: pitch angular rate, predicted using kalman filter
	 * 	 p_pias: pitch error, output of kalman filter
	 * 	 phi: integration of pitch_r to predict acceleration value (sphi)
	 */
	static int pitch_r = 0, p_bias = 0, phi = 0;
	int new_pitch, acc_pitch = FILTER_VAL(SENS_ACC_PITCH, PID_ACC_SCALAR), pitchs = 0;

	// Kalman for pitch_r, phi
	//calculate actual expected pitch rate
	#if DATA_SRC == FILTER_DATA
		pitchs = fp_from_int(pitch, PID_PREC);
		pitch_r = FILTER_VAL(SENS_GYRO_PITCH, PID_GYRO_SCALAR) - p_bias;
		//kalman filter prediction, phi
		phi = phi + fp_mul_fi(pitch_r, P2PHI, PID_PREC);
		//corrections based on constants C1 and C2
		phi = phi - ((phi - acc_pitch) / C1pr);
		p_bias = p_bias + ((phi - acc_pitch) / C2pr);
	#else
		pitchs = pitch;
		pitch_r = fp_from_int(FILTER_VAL(SENS_GYRO_PITCH, PID_GYRO_SCALAR), PID_PREC) - p_bias;
		//kalman filter prediction, phi
		phi = phi + fp_mul_fi(pitch_r, P2PHI, PID_PREC);
		//corrections based on constants C1 and C2, no fixed point division needed because C1/C2 are integers
		phi = phi - ((phi - fp_from_int(acc_pitch, PID_PREC)) / C1pr);
		p_bias = p_bias + ((phi - fp_from_int(acc_pitch, PID_PREC)) / C2pr);
	#endif

	// Use p, phi in cascaded P controller
	new_pitch = fp_mul_fi(P1pr.n, (pitchs - phi), PID_PREC) - fp_mul_fi(P2pr.n, pitch_r, PID_PREC);

	cpitch = fp_int_i(new_pitch, PID_PREC);
}

/*
 * Author: Corniël Joosse
 * Exactly the same function as _pid_pitch_controller, it is not one function because they have their own static variables,
 * and using a struct to save all the data for pitch and roll separately is slower.
 */
static void _pid_roll_controller() {
	static int roll_r = 0, p_bias = 0, phi = 0;
	int new_roll, acc_roll = FILTER_VAL(SENS_ACC_ROLL, PID_ACC_SCALAR), rolls = 0;

	// Kalman for roll_r, phi
	//calculate actual expected roll rate
	#if DATA_SRC == FILTER_DATA
		rolls = fp_from_int(roll, PID_PREC);
		roll_r = FILTER_VAL(SENS_GYRO_ROLL, PID_GYRO_SCALAR) - p_bias;
		//kalman filter prediction, phi
		phi = phi + fp_mul_fi(roll_r, P2PHI, PID_PREC);
		//corrections based on constants C1 and C2
		phi = phi - ((phi - acc_roll) / C1pr);
		p_bias = p_bias + ((phi - acc_roll) / C2pr);
	#else
		rolls = roll;
		roll_r = fp_from_int(FILTER_VAL(SENS_GYRO_ROLL, PID_GYRO_SCALAR), PID_PREC) - p_bias;
		//kalman filter prediction, phi
		phi = phi + fp_mul_fi(roll_r, P2PHI, PID_PREC);
		//corrections based on constants C1 and C2, no fixed point division needed because C1/C2 are integers
		phi = phi - ((phi - fp_from_int(acc_roll, PID_PREC)) / C1pr);
		p_bias = p_bias + ((phi - fp_from_int(acc_roll, PID_PREC)) / C2pr);
	#endif

	// Use p, phi in cascaded P controller
	new_roll = fp_mul_fi(P1pr.n, (rolls - phi), PID_PREC) - fp_mul_fi(P2pr.n, roll_r, PID_PREC);

	croll = fp_int_i(new_roll, PID_PREC);
}

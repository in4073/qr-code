#include "main.h"
#include "Scheduler.h"
#include "engines.h"
#include "uart.h"
#include "debug.h"
#include "filter.h"
#include "calibration.h"
#include "fixedpoint.h"
#include "Logger/status_logger.h"
#include "exceptions.h"

//static allocation of memmory for malloc calls
char malloc_memory[1024];
int malloc_memory_size = 1024;



/**
 * Author: Hajo Kleingeld
 * Start of the program, inits evrything, then starts the acutal program.
 */
int main() {
	int test;
	filter_init();
	uart_init();
	Engines_init();
	exceptions_init();

	// start the fireworks
	ENABLE_INTERRUPT(INTERRUPT_GLOBAL);
	debug_printf("Communication established!");

	startProgram();

	DISABLE_INTERRUPT(INTERRUPT_GLOBAL);
	return 0;
}

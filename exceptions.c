#include <x32.h>
#include <limits.h>
#include "debug.h"
#include "protocol.h"
#include "uart.h"

/*
 * Author: Corniël Joosse
 */
void isr_exc_divbyzero() {
	debug_printf("EXCEPTION division by 0");
}

/*
 * Author: Corniël Joosse
 */
void isr_exc_trap() {
	debug_printf("EXCEPTION trap");
}

/*
 * Author: Corniël Joosse
 */
void isr_exc_overflow() {
	debug_printf("EXCEPTION overflow");
}

/*
 * Author: Corniël Joosse
 */
void isr_exc_outofmemory() {
	debug_printf("EXCEPTION out of mem");
}

/*
 * Author: Corniël Joosse
 * Some test cases to trigger the exception interrupts. The trap interrupt is untested.
 */
void exceptions_test() {
	int a = 0, b = INT_MAX;
	char *p = (char *)0x12345678;
	void (*func_ptr)(void) = (void (*)(void))0x8123;

	//overflow
	b += 2^10;

	//div by 0
	b = b/a;

	//out of mem
	*p = 1;

	//trap -> test not working...
	func_ptr();

	//exit(-1);
}

/**
 * Author: Corniël Joosse
 * Initializes all exception isr's, the isr's only send a debug message.
 */
void exceptions_init() {
	SET_INTERRUPT_VECTOR(INTERRUPT_DIVISION_BY_ZERO, &isr_exc_divbyzero);
	SET_INTERRUPT_PRIORITY(INTERRUPT_DIVISION_BY_ZERO, 30);
	ENABLE_INTERRUPT(INTERRUPT_DIVISION_BY_ZERO);

	SET_INTERRUPT_VECTOR(INTERRUPT_TRAP, &isr_exc_trap);
	SET_INTERRUPT_PRIORITY(INTERRUPT_TRAP, 31);
	ENABLE_INTERRUPT(INTERRUPT_TRAP);

	/* Overflow isr is disabled because it is also triggered by right shifts
	 * and checksum calculation (with overflows which are actually wanted)
	 */
	//SET_INTERRUPT_VECTOR(INTERRUPT_OVERFLOW, &isr_exc_overflow);
	//SET_INTERRUPT_PRIORITY(INTERRUPT_OVERFLOW, 32);
	//ENABLE_INTERRUPT(INTERRUPT_OVERFLOW);

	SET_INTERRUPT_VECTOR(INTERRUPT_OUT_OF_MEMORY, &isr_exc_outofmemory);
	SET_INTERRUPT_PRIORITY(INTERRUPT_OUT_OF_MEMORY, 33);
	ENABLE_INTERRUPT(INTERRUPT_OUT_OF_MEMORY);
}

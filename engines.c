#include "main.h"
#include "engines.h"
#include "x32.h"
#include "Logger/SensorLogger.h"
#include "debug.h"
#include "filter.h"
#include "protocol.h"
#include "pidtester.h"
#include "LazyTimer/LazyTimer.h"
#include "EngineControl/EngineControl.h"

Engine e1, e2, e3, e4;

LazyTimer panicTimer, ledTimer;

/*
 * Author: Hajo Kleingeld
 * Main timer ISR, Stores the sensor values 500x a second and kicks the mainloop
 * It used to drive the engines as well, but this was changed during the final stages of the project.
 * Has not been moved since.
 */
void isr_timer(void)
{
		static unsigned int i = 0;

		#ifdef TEST_MODE
			//use fake sensor data from pidtester, test_read_index is incremented in pidtester.c:set_next_setpoint()
			sensors[0][SensorWriteIndex] = pidtest_get_sensor(0);
			sensors[1][SensorWriteIndex] = pidtest_get_sensor(1);
			sensors[2][SensorWriteIndex] = pidtest_get_sensor(2);
			sensors[3][SensorWriteIndex] = pidtest_get_sensor(3);
			sensors[4][SensorWriteIndex] = pidtest_get_sensor(4);
			sensors[5][SensorWriteIndex] = pidtest_get_sensor(5);
		#else
			sensors[0][SensorWriteIndex] = X32_QR_s0;
			sensors[1][SensorWriteIndex] = X32_QR_s1;
			sensors[2][SensorWriteIndex] = X32_QR_s2;
			sensors[3][SensorWriteIndex] = X32_QR_s3;
			sensors[4][SensorWriteIndex] = X32_QR_s4;
			sensors[5][SensorWriteIndex] = X32_QR_s5;
		#endif

		timestamps[SensorWriteIndex] = X32_us_clock;

    	(SensorWriteIndex == SENSOR_BUFFER_SIZE-1)?(SensorWriteIndex = 0):(SensorWriteIndex++);
    	KickMain = true;
}

/*
 * Author: Hajo Kleingeld
 * Panic ISR. The program will switch to this ISR if the QR goes into panic mode.
 * This code controlls the engines during that time.
 */
void isr_panic_timer(void)
{
	(e1.real != 0) ? (e1.real--) : (e1.real = 0);
	(e2.real != 0) ? (e2.real--) : (e2.real = 0);
	(e3.real != 0) ? (e3.real--) : (e3.real = 0);
	(e4.real != 0) ? (e4.real--) : (e4.real = 0);

	X32_QR_a0 = e1.real;
	X32_QR_a1 = e2.real;
	X32_QR_a2 = e3.real;
	X32_QR_a3 = e4.real;

	KickMain = true;
}

/*
 * Author: Hajo Kleingeld
 * This code inits the engines.
 */
void Engines_init(void){
	e1.EngineSetpoint = 0;
	e2.EngineSetpoint = 0;
	e3.EngineSetpoint = 0;
	e4.EngineSetpoint = 0;

	e1.real = 0;
	e2.real = 0;
	e3.real = 0;
	e4.real = 0;

	/*Copy pasted from example*/
    X32_timer_per = 2 * CLOCKS_PER_MS;
    SET_INTERRUPT_VECTOR(INTERRUPT_TIMER1, &isr_timer);
    SET_INTERRUPT_PRIORITY(INTERRUPT_TIMER1, 22);
    ENABLE_INTERRUPT(INTERRUPT_TIMER1);
}

/*
 * Author: Hajo Kleingeld
 * Will be called if the QR switches to panic mode. It changes the ISR to the panic ISR
 * and sets several variables needed for the panic mode.
 */
void EnginePANIC(void){
    DISABLE_INTERRUPT(INTERRUPT_TIMER1);
    X32_timer_per = 1 * CLOCKS_PER_MS;
    SET_INTERRUPT_VECTOR(INTERRUPT_TIMER1, &isr_panic_timer);
    ENABLE_INTERRUPT(INTERRUPT_TIMER1);

    Liftoff = false;
	SetEngineValues (0,0,0,0);
	ResetTimer(&panicTimer);
	ResetTimer(&ledTimer);
}

/*
 * Author: Hajo Kleingeld
 * Code witch is called from the schedeuler when in panic moce.
 */
bool RunPanic(void){
	if(EnginesOff() && pc_connected) {
		if(runTimer(&panicTimer, 2000)){
			Engines_init();
			debug_printf("Panic resolved.");
			led_all_off(0xF0);
			ResetTimer(&panicTimer);
			return true;
		}
	} else if(runTimer(&ledTimer, 500)) {
		//blink all leds during panic!
		led_all_toggle(0xF0);
		ResetTimer(&ledTimer);
	}
	return false;
}

/*
 * Author: Hajo Kleingeld
 * This function, and only this function should be used to set the engine value (exept for panic mode)
 */
void SetEngineValues(unsigned short p1, unsigned short p2, unsigned short p3, unsigned short p4){

	if(Liftoff){
		e1.real = p1;
		e2.real = p2;
		e3.real = p3;
		e4.real = p4;
	}
	else{
		if(e1.real > p1) {
			e1.real -= ENGINE_ADJUSTMENT;
		} else if(e1.real < p1) {
			e1.real += ENGINE_ADJUSTMENT;
		}

		if(e2.real > p2) {
			e2.real -= ENGINE_ADJUSTMENT;
		} else if(e2.real < p2) {
			e2.real += ENGINE_ADJUSTMENT;
		}

		if(e3.real > p3) {
			e3.real -= ENGINE_ADJUSTMENT;
		} else if(e3.real < p3) {
			e3.real += ENGINE_ADJUSTMENT;
		}

		if(e4.real > p4) {
			e4.real -= ENGINE_ADJUSTMENT;
		} else if(e4.real < p4) {
			e4.real += ENGINE_ADJUSTMENT;
		}
	}
	X32_QR_a0 = e1.real;
	X32_QR_a1 = e2.real;
	X32_QR_a2 = e3.real;
	X32_QR_a3 = e4.real;
}

/*
 * Author: Hajo Kleingeld
 * Check if all engines are off
 */
bool EnginesOff(void){
	if(e1.real || e2.real || e3.real || e4.real){
		return false;
	}
	return true;
}

/*
 * Author: Corniël Joosse
 * Prints the engine values, convinient for debugging.
 */
void printEngineValues() {
	debug_printf("Engines: %d %d %d %d (%d)", e1.real, e2.real, e3.real, e4.real, EnginesOff());
}

/*
 * Author: Hajo Kleingeld
 * returns engine 1 current value
 */
unsigned short getEngine1(void){
	return e1.real;
}

/*
 * Author: Hajo Kleingeld
 * returns engine 2 current value
 */
unsigned short getEngine2(void){
	return e2.real;
}

/*
 * Author: Hajo Kleingeld
 * returns engine 3 current value
 */
unsigned short getEngine3(void){
	return e3.real;
}

/*
 * Author: Hajo Kleingeld
 * returns engine 4 current value
 */
unsigned short getEngine4(void){
	return e4.real;
}

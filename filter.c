#define FILTER_GLOBAL_DEFENITIONS
#include "filter.h"
#include "main.h"
#include "string.h"
#include "fixedpoint.h"
#include "calibration.h"
#include "debug.h"

/*sample rate = 500Hz, 10 bit precision, 2nd order, 10Hz*/
#define P_2_10 10

#define A0_2_10 0x00000004
#define A1_2_10 0x00000007
#define A2_2_10 0x00000004
#define B1_2_10 0xfffff8b6
#define B2_2_10 0x00000359

/*sample rate = 500Hz, 10 bit precision, 1st order, 10Hz*/

#define P_1_10 12
#define A0_1_10 0x000000f2
#define A1_1_10 0x000000f2
#define B1_1_10 0xfffff1e5

#define P_1_25 12
#define A0_1_25 0x00000230
#define A1_1_25 0x00000230
#define B1_1_25 0xfffff460

/*
 * Author: Hajo Kleingeld
 * Code witch inits the filters. Basically just sets all values in all arrays used to zero.
 */
void filter_init(void){
	memset(&sensors[0][0],0,sizeof(sensors));
	memset(&filterOutput[0][0],0,sizeof(filterOutput));
	memset(&filterReadIndex[0],0,sizeof(filterReadIndex));

	memset(&x0[0],0,sizeof(x0));
	memset(&x1[0],0,sizeof(x1));
	memset(&x2[0],0,sizeof(x2));
	memset(&y1[0],0,sizeof(y1));
	memset(&y2[0],0,sizeof(y2));

	SensorWriteIndex = 0;
}

/*
 * Author: Hajo Kleingeld
 * returns the amount of samples that the filter currently lacks behind the sensors.
 */
int getFilterLack(void){
	if(filterReadIndex[0] < SensorWriteIndex){
		return SensorWriteIndex - filterReadIndex[0];
	}
	return SensorWriteIndex + (SENSOR_BUFFER_SIZE-1) - filterReadIndex[0];
}

/*
 * Author: Hajo Kleingeld
 * Make all filters skip one sensor sample.
 */
void skipSampleFilter(void){
	int currVal = filterReadIndex[0];

	(currVal == SENSOR_BUFFER_SIZE-1) ? (currVal = 0):(currVal++);

	filterReadIndex[0] = currVal;
	filterReadIndex[1] = currVal;
	filterReadIndex[2] = currVal;
	filterReadIndex[3] = currVal;
	filterReadIndex[4] = currVal;
	filterReadIndex[5] = currVal;
}

/*
 * Author: Hajo Kleingeld
 * Runs a second order butterworth filter with cutoff frequency 10, is no longer used.
 * Returns true if another sample is avaidable from the sensors.
 */
bool runButterworth_2_10(int SensorNr){
	uint16_t readIndex = filterReadIndex[SensorNr]; /*loaded into variable for readability and speed*/

	if(readIndex != SensorWriteIndex){
		x0[SensorNr] = fp_from_int(sensors[SensorNr][readIndex] - sensor_offsets[SensorNr],P_2_10);

		filterOutput[SensorNr][readIndex] = fp_mul_fsi2(A0_2_10, x0[SensorNr], P_2_10)
								           +fp_mul_fsi2(A1_2_10, x1[SensorNr], P_2_10) + fp_mul_fsi2(A2_2_10 ,x2[SensorNr], P_2_10)
					 			           -fp_mul_fsi2(B1_2_10 ,y1[SensorNr], P_2_10) - fp_mul_fsi2(B2_2_10, y2[SensorNr], P_2_10);
		x2[SensorNr] = x1[SensorNr];
		x1[SensorNr] = x0[SensorNr];
		y2[SensorNr] = y1[SensorNr];
		y1[SensorNr] = filterOutput[SensorNr][readIndex];

		(readIndex == SENSOR_BUFFER_SIZE-1)?(filterReadIndex[SensorNr] = 0):(filterReadIndex[SensorNr]++); /* write back loaded read index */

		return true;
	}
	else{
		return false;
	}
}

/*
 * Author: Hajo Kleingeld
 * Runs a frist butterworth filter with cutoff frequency 10.
 * Returns true if another sample is avaidable from the sensors.
 */
bool runButterworth_1_10(int SensorNr){
	uint16_t readIndex = filterReadIndex[SensorNr]; /*loaded into variable for readability and speed*/

	if(readIndex != SensorWriteIndex){
		x0[SensorNr] = fp_from_int(sensors[SensorNr][readIndex] - sensorBaseOffsets[SensorNr],P_1_10) - sensor_offsets[SensorNr];

		filterOutput[SensorNr][readIndex] = fp_mul_fsi3(A0_1_10, x0[SensorNr], P_1_10) +
											fp_mul_fsi3(A1_1_10, x1[SensorNr], P_1_10) -
											fp_mul_fsi3(B1_1_10, y1[SensorNr], P_1_10);

		x1[SensorNr] = x0[SensorNr];
		y1[SensorNr] = filterOutput[SensorNr][readIndex];

		(readIndex == SENSOR_BUFFER_SIZE-1)?(filterReadIndex[SensorNr] = 0):(filterReadIndex[SensorNr]++); /* write back loaded read index */

		return true;
	}
	else{
		return false;
	}
}

/*
 * Author: Hajo Kleingeld
 * Runs a frist butterworth filter with cutoff frequency 25.
 * Returns true if another sample is avaidable from the sensors.
 */
bool runButterworth_1_25(int SensorNr){
	uint16_t readIndex = filterReadIndex[SensorNr]; /*loaded into variable for readability and speed*/

	if(readIndex != SensorWriteIndex){
		x0[SensorNr] = fp_from_int(sensors[SensorNr][readIndex] - sensorBaseOffsets[SensorNr],P_1_25) - sensor_offsets[SensorNr];

		filterOutput[SensorNr][readIndex] = fp_mul_fsi3(A0_1_25, x0[SensorNr], P_1_25) +
											fp_mul_fsi3(A1_1_25, x1[SensorNr], P_1_25) -
											fp_mul_fsi3(B1_1_25, y1[SensorNr], P_1_25);

		x1[SensorNr] = x0[SensorNr];
		y1[SensorNr] = filterOutput[SensorNr][readIndex];

		(readIndex == SENSOR_BUFFER_SIZE-1)?(filterReadIndex[SensorNr] = 0):(filterReadIndex[SensorNr]++); /* write back loaded read index */

		return true;
	}
	else{
		return false;
	}
}

/*
 *
 * Function witch gets called form the scheduller
 * Runs each nessesary filter once.
 */
void RunFilters(void){
	runButterworth_1_25(0);
	runButterworth_1_25(1);
	runButterworth_1_25(2);
	runButterworth_1_25(3);
	runButterworth_1_25(4);
	runButterworth_1_25(5);
}

/*
 * Author: Hajo Kleingeld
 * Run filters until the all buffered values are used
 * Don't use this if you want to be sure its done in time. Use it for syncing instead.
 * This function can take up to 0.1 s to execute!
 */
void runFiltersFull(void){
	while(SensorWriteIndex != filterReadIndex[0])
	{
		while(runButterworth_1_25(0));
		while(runButterworth_1_25(1));
		while(runButterworth_1_25(2));
		while(runButterworth_1_25(3));
		while(runButterworth_1_25(4));
		while(runButterworth_1_25(5));
	}
}

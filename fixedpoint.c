#include "main.h"
#include "debug.h"
#include "fixedpoint.h"

/*
 * Author: Corniël Joosse
 * A constructor to construct a FixedPoint number. N should already be in fixed point notation.
 * Use fp(N,P) instead to get a correctly formated fp number.
 */
FixedPoint fp_cons(int N, int P) {
	FixedPoint R;
	R.n = N;
	R.p = P;
	return R;
}

/*
 * Author: Corniël Joosse
 * Multiplies two FixedPoint numbers, returns a pointer (for nesting) to R in which result is also placed.
 * Resulting precision is precision from A
 */
FixedPoint* fp_mul(FixedPoint* R, FixedPoint* A, FixedPoint* B) {
	R->p = A->p;

	if(R->p != B->p) {
		fp_conv(B, R->p);
	}

	R->n = (A->n >> (A->p/2)) * (B->n >> (A->p/2));
	return R;
}

/*
 * Author: Corniël Joosse
 * Divides two FixedPoint numbers, returns a pointer (for nesting) to R in which result is also placed.
 */
FixedPoint* fp_div(FixedPoint* R, FixedPoint* A, FixedPoint* B) {
	static int half_ap;
	R->p = A->p;

	if(A->p != B->p) {
		fp_conv(B, A->p);
	}

	half_ap = A->p/2;
	if(A->n >> (half_ap*3)) {
		// shift out fraction, and half precision of the integer. if there are still bits 1, use less precise method
		R->n = ((A->n) / B->n) << A->p;
	} else {
		// else, use more precise method
		R->n = ((A->n << A->p/2) / B->n) << (A->p/2);
	}


	return R;
}

/*
 * Author: Corniël Joosse
 * Calculates the power of an FixedPoint number, power can only be integers.
 * Returns a pointer (for nesting) to R in which result is also placed.
 */
FixedPoint* fp_pow(FixedPoint* R, FixedPoint* A, int pow) {
	int i;
	R->n = A->n;
	R->p = A->p;

	if(pow == 0) {
		A->n = 0;
	}
	else if(pow == 1) {
		//R=A, so return current value of A
		return R;
	}
	else {
		for(i = 0; i < pow-1; i++) {
			fp_mul(R, R, A);
		}
	}
	return R;
}

/*
 * Author: Corniël Joosse
 * Adds two FP numbers, returns a pointer (for nesting) to R in which result is also placed.
 * If used without nesting, it is also possible to use R.n = A.n + B.n.
 */
FixedPoint* fp_plus(FixedPoint* R, FixedPoint* A, FixedPoint* B) {
	if(A->p != B->p) {
		fp_conv(B, A->p);
	}
	R->p = A->p;
	R->n = A->n + B->n;
	return R;
}

/*
 * Author: Corniël Joosse
 * Subtracts two FP numbers, returns a pointer (for nesting) to R in which result is also placed.
 * If used without nesting, it is also possible to use R.n = A.n - B.n.
 */
FixedPoint* fp_min(FixedPoint* R, FixedPoint* A, FixedPoint* B) {
	if(A->p != B->p) {
		fp_conv(B, A->p);
	}
	R->p = A->p;
	R->n = A->n - B->n;
	return R;
}

/*
 * Author: Corniël Joosse
 * Converts FixedPoint to given precision
 */
void fp_conv(FixedPoint* A, int precision) {
	int shift = A->p - precision;
	if(shift < 0) {
		A->n = A->n << -1*shift;
	} else {
		A->n = A->n >> shift;
	}
}

/*
 * Author: Corniël Joosse
 * Converts FixedPoint to a string with decimal number.
 */
void fp_print(FixedPoint* A, char *str) {
	uint32_t pos, i, mask, frac, frac_space;

	// print integer part, easy...
	pos = sprintf(str, "%d", fp_int(A));

	// due to the way the fraction part is saved, this is a bit harder

	// calculate the space in which we store the fraction for now, byte aligned
	frac_space = ((A->p - 1) / 8 + 1) * 8;

	// create a mask of 1's for fraction space
	mask = fp_mask(frac_space);

	// get the fraction and shift it so it is aligned to beginning of frac_space
	frac = fp_frac(A->n, A->p) << (frac_space - A->p);

	// for each byte a decimal is printed
	i = frac_space/4;
	if(frac != 0 && i > 0) {
		str[pos++] = '.';
	}

	// get the fraction which is left inside the mask, multiply with 10,
	// and get the value by shifting out the fraction space
	while(frac != 0 && i > 0) {
		frac = (frac & mask) * 10;
		str[pos++] = '0' + (frac >> frac_space) % 10;
		i--;
	}

	//terminate the string
	str[pos] = '\0';
}

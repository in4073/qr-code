#include "debug.h"
#include "main.h"
#include "uart.h"
#include "protocol.h"
#include <stdio.h>


byte debug_buffer[DEBUG_BUFFER_SIZE] = {0};

/*
 * Author: Corniël Joosse
 * This function is used by _xprintf() to process the printf parameters and is copied from stdio.c.
 */
static int _pc(int chr, void* param) {
	**(char**)param = (char)chr;
	*((char**)param) = *((char**)param)+1;
	return 1;
}

/*
 * Author: Corniël Joosse
 * A printf function that sends the resulting string directly to the send buffer using a proper protocol message.
 * If DEBUG_MODE == DEBUG_MODE_DIRECT the message is printed to the serial port directly.
 */
void debug_printf(const char* fmt, ...) {
	//Message *message;
	va_list ap;
	int ret;
	void *vbuffer = (void*)debug_buffer;

	// start reading arguments
	va_start(ap, fmt);

	ret = _xprintf(_pc, (void*)&vbuffer, fmt, &ap);
	debug_buffer[ret] = '\0';

	// stop reading parameters
	va_end(ap);

	// send a message using the protocol
	//message = protocol_create_message(TYPE_DEBUG, ret, debug_buffer);
	#if DEBUG_MODE == DEBUG_MODE_DIRECT
		printf("%s\n\r", debug_buffer);
	#else
		if(uart_outbuffer_space() > 40) {
			protocol_send_new_message(TYPE_DEBUG, ret, debug_buffer);
		}
	#endif
}

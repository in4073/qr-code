#ifndef _FILTER_H_GUARD_
#define _FILTER_H_GUARD_
#include "main.h"

#undef EXTERN /*nessesary if more .h files with global declarations are in the code*/
#ifdef FILTER_GLOBAL_DEFENITIONS
#define EXTERN
#else
#define EXTERN extern
#endif

#define SENSOR_BUFFER_SIZE 100

#define SENS_ACC_PITCH		0
#define SENS_ACC_ROLL		1
#define SENS_ACC_YAW		2
#define SENS_GYRO_ROLL		3
#define SENS_GYRO_PITCH		4
#define SENS_GYRO_YAW		5

#define FILTER_PREC			12

EXTERN unsigned char SensorWriteIndex;
EXTERN short sensors[6][SENSOR_BUFFER_SIZE];
EXTERN unsigned int timestamps[SENSOR_BUFFER_SIZE];

EXTERN int filterOutput[6][SENSOR_BUFFER_SIZE];
EXTERN int x0[6];
EXTERN int x1[6];
EXTERN int x2[6];
EXTERN int y1[6];
EXTERN int y2[6];
EXTERN unsigned char filterReadIndex[6];

EXTERN unsigned int KickMain;

void filter_init(void);
bool runButterworth_2_10(int SensorNr);
void RunFilters(void);
void runFiltersFull(void);
void skipSampleFilter(void);
int getFilterLack(void);

#endif

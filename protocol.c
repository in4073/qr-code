#include "main.h"
#include "uart.h"
#include "protocol.h"
#include "debug.h"
#include "statemachine.h"
#include "pidcontroller.h"
#include "EngineControl/EngineControl.h"
#include "Logger/SensorLogger.h"
#include "Logger/status_logger.h"
#include "LazyTimer/LazyTimer.h"
#include <stdlib.h>
#include <stdio.h>

byte MessageTypeSize[] = {5, 1, 10, 30, 26, 1, 0, 0, 28, 14};
bool pc_connected = 0;

void RunProtocol(void){
	static LazyTimer com_timeout_timer;

	if(protocol_parse_message()){
		ResetTimer(&com_timeout_timer);
		led_toggle(LED_PC_COMM);
		pc_connected = true;
	}
	else{
		if(runTimer(&com_timeout_timer, 500)) {
			led_off(LED_PC_COMM);
			pc_connected = false;
			PANIC();
		}
	}
}

/*
 * Author: Corniël Joosse
 * Check the fifo buffer if it contains a valid message.
 * Bytes before the start byte are discarded.
 */
static bool _protocol_check_message() {
	char c, type;
	int message_size;

	if(uart_buffer_size() < 3) {
		return false;
	}

	//delete received bytes until START_BYTE is found, message always starts with a start byte
	c = uart_get_byte(START_BYTE);
	while (c != START_BYTE_VALUE) {
		//if buffer is empty, uart_delete_byte will return false
		if(!uart_delete_byte(1)) {
			break;
		}
		debug_printf("No start byte (0x%2x)", c);

		c = uart_get_byte(START_BYTE);
	}


	//check the size of the message
	type = uart_get_byte(MESSAGE_TYPE_BYTE);

	//check if type exists and is not zero
	if(type <= 0 || type > sizeof(MessageTypeSize)) {
		//if type not exists, remove bytes
		debug_printf("Wrong type (%d)", type);
		uart_delete_byte(MESSAGE_TYPE_BYTE);
		return false;
	}


	message_size = protocol_message_size(type);
	//debug_printf("payl: %d, mess: %d \r\n", message_size, uart_buffer_size());

	if(uart_buffer_size() >= MIN_MESSAGE_SIZE + message_size) {
		return true;
	}

	return false;
}

/*
 * Author: Corniël Joosse
 * Parses a message in the receive buffer and calls the appropriate function of the right module based on the message type.
 * A message consists of:
 * 1) a start byte (0x4D)
 * 2) a message type byte, determines the payload type and length
 * 3) the payload
 * 4) a checksum, a sum of all data except the start byte
 */
bool protocol_parse_message() {
	// init the variables,
	byte message_type, checksum_calc, checksum_mes;
	char c;
	int i, plength;
	bool checksum_right = false;

	//check if there could be a message
	if(!_protocol_check_message()) {
		return false;
	}

	//get the message type, start byte is ignored and already check by _protocol_check_message()
	message_type = uart_get_byte(MESSAGE_TYPE_BYTE);

	//debug_printf("type: %d", message->type);
	plength = protocol_message_size(message_type);

	//Start with message type, and check payload length + 1 (1 for the message type byte)
	checksum_calc = uart_checksum(MESSAGE_TYPE_BYTE, plength+1);
	checksum_mes = uart_get_byte(PAYLOAD_BYTE+plength);
	if(checksum_calc == checksum_mes) {
		checksum_right = true;
	}

	//create a new pointer to the message in a different variable with the desired type and fill the content
	switch(message_type) {
		case TYPE_CONTROL:
			if(checksum_right) {
				//disable control from pc in test mode, data is set by pidtester.c
				#ifndef TEST_MODE
					//call other modules function, control bytes are send in order: lift, roll, pitch, yaw
					SetControls(
						uart_get_byte(PAYLOAD_BYTE+2),
						uart_get_byte(PAYLOAD_BYTE+3),
						uart_get_byte(PAYLOAD_BYTE+4),
						word(uart_get_byte(PAYLOAD_BYTE),uart_get_byte(PAYLOAD_BYTE+1))
					);
				#endif

			} else {
				//message is discarded
				//debug_printf("checksum rec: %d calc: %d", checksum_mes, checksum_calc);
			}

			break;
		case TYPE_MODE:
			//debug_printf("Mode msg received: %d", message_mode->mode);

			if(checksum_right) {
				//call other modules function, only one byte in payload which is mode
				setState(uart_get_byte(PAYLOAD_BYTE));
				protocol_ack(TYPE_MODE);
			} else {
				//message is discarded
				//debug_printf("checksum rec: %d calc: %d", message->checksum, checksum);
			}

			break;

		case TYPE_ALGO_PARAMS:
			if(checksum_right) {
				//ack message
				protocol_ack(TYPE_ALGO_PARAMS);

				//call other modules function, order of bytes Py, Dy, P1pr, P2pr, C1pr, C2pr
				pid_set_params(
					word(uart_get_byte(PAYLOAD_BYTE),uart_get_byte(PAYLOAD_BYTE+1)),
					word(uart_get_byte(PAYLOAD_BYTE+2),uart_get_byte(PAYLOAD_BYTE+3)),
					word(uart_get_byte(PAYLOAD_BYTE+4),uart_get_byte(PAYLOAD_BYTE+5)),
					word(uart_get_byte(PAYLOAD_BYTE+6),uart_get_byte(PAYLOAD_BYTE+7)),
					word(uart_get_byte(PAYLOAD_BYTE+8),uart_get_byte(PAYLOAD_BYTE+9))
				);

			} else {
				//message is discarded
				//debug_printf("checksum rec: %d calc: %d", message->checksum, checksum);
			}

			break;

		case TYPE_LOG_START:
			if(checksum_right) {
				//ack message
				protocol_ack(TYPE_LOG_START);

				//checksum is right, so call log module function to start logging
				SensorLoggerStart();
				status_logger_start();
			}
			break;

		case TYPE_LOG_REQUEST:
			if(checksum_right) {
				//ack message
				protocol_ack(TYPE_LOG_REQUEST);

				//checksum is right, so call log module function to start sending log
				debug_printf("Log requested");
				setState(SEND_LOG_MODE);
			}
			break;

		case TYPE_LOG_SENSORS:
		case TYPE_LOG_TELEMETRY:
		case TYPE_DEBUG:
		case TYPE_TELEMTERY:
		case TYPE_ACK:
		default:
			//only send by fpga and receiving implemented on PC
			break;
	}

	//remove this message from the buffer: start byte (1), message type (1), payload (~) and checksum (1)
	uart_delete_byte(PAYLOAD_BYTE + plength + 1);

	//printf("after fetch:\n\r");
	//uart_print();

	return checksum_right;
}

/*
 * Author: Corniël Joosse
 * Calculates the checksum using the given type and a pointer to the payload.
 */
int protocol_calc_checksum(int type, byte* payload, int length) {
	//init vars, also sum type
	byte sum = type;
	int i;

	// access the message struct like an array, byte by byte
	for(i = 0; i < length; i++) {
		//sum the bytes, because we use a byte as type, it will overflow many times, which is what we need
		sum += payload[i];
	}

	return sum;
}

/*
 * Author: Corniël Joosse
 * Creates a message struct, is not used a lot because it is slower than using protocol_send_new_message()
 */
Message* protocol_create_message(byte message_type, int plength, void *payload) {
	byte* messageptr = malloc(sizeof(Message));
	Message* message = (Message *)messageptr;
	int i, mlength;

	message->type = message_type;
	mlength = protocol_message_size(message->type);

	if(plength > mlength) {
		plength = mlength;
	}

	message->payload = payload;
	return message;
}

/*
 * Author: Corniël Joosse
 * Send a fixed message using a variable amount of parameters, calculates the checksum from the type and payload.
 */
void protocol_send_fixed_message(byte type, int payload_size, ...) {
	int i;
	byte b;
	byte checksum = type;
	va_list ap;

	uart_send_byte(START_BYTE_VALUE);
	uart_send_byte(type);

	va_start(ap, payload_size);
	for(i = 0; i < payload_size; i++) {
		b = va_arg(ap, byte);
		checksum += b;
		uart_send_byte(b);
	}

	uart_send_byte(checksum);

	va_end(ap);
}

/*
 * Author: Corniël Joosse
 * Sends the message from a message struct, is not used a lot because it is slower than using protocol_send_new_message()
 */
void protocol_send_message(Message *message, int plength) {
	int i, mlength;
	byte checksum;

	//check type and message/payload length
	mlength = protocol_message_size(message->type);

	//if payload size is larger than max message size, cut of the message
	if(plength > mlength) {
		plength = mlength;
	}

	//calculate the checksum
	message->checksum = protocol_calc_checksum(message->type, message->payload, plength);

	//send the message
	uart_send_byte(START_BYTE_VALUE);
	uart_send_byte(message->type);

	//send the payload
	for(i = 0; i < plength; i++) {
		uart_send_byte(message->payload[i]);
	}

	//send 0-bytes if the payload is shorter than message size
	for(; i < mlength; i++) {
		uart_send_byte(0);
	}

	//send the checksum
	uart_send_byte(message->checksum);

	//free the memory
	free(message);
}

/*
 * Author: Corniël Joosse
 * Sends a new message without using any struct based on the type, length and the payload from the payload pointer.
 */
void protocol_send_new_message(byte message_type, int plength, void *payload) {
	int i, mlength;
	byte checksum, *payl_ptr = payload;

	//check type and message/payload length
	mlength = protocol_message_size(message_type);

	//if payload size is larger than max message size, cut of the message
	if(plength > mlength) {
		plength = mlength;
	}

	//send the message and add to checksum
	uart_send_byte(START_BYTE_VALUE);
	uart_send_byte(message_type);
	checksum = message_type;

	//send the payload and calculate checksum
	for(i = 0; i < plength; i++) {
		uart_send_byte(payl_ptr[i]);
		checksum += payl_ptr[i];
	}

	//send 0-bytes if the payload is shorter than message size
	for(; i < mlength; i++) {
		uart_send_byte(0);
	}

	//send the checksum
	uart_send_byte(checksum);
}

/*
 * Author: Corniël Joosse
 * Send an acknowledgment to acknowledge a message of the given type.
 */
void protocol_ack(char type) {
	//send the acknowledgment
	uart_send_byte(START_BYTE_VALUE);
	uart_send_byte(TYPE_ACK);
	uart_send_byte(type);
	uart_send_byte((uint8_t)(TYPE_ACK + type));

}

/*
 * Author: Corniël Joosse
 * Get the payload size of a certain message type.
 */
int protocol_message_size(int type) {
	if((type > 0) && (type <= sizeof(MessageTypeSize))) {
		return MessageTypeSize[type-1];
	}
	return 0;
}

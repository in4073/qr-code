#include "main.h"

#ifndef UART_H
#define UART_H

	// RX FIFO
	#define BUFFERSIZE 300

	#define SND_SEQUENTLY_BYTES		10

	// define the functions
	void uart_init(void);
	void isr_rs232_rx(void);
	void isr_rs232_tx(void);
	//void isr_wireless_rx(void);
	void isr_xufo(void);

	int uart_outbuffer_space(void);
	int uart_buffer_size(void);
	int uart_outbuffer_size(void);
	bool uart_delete_byte(int);
	void uart_print(void);
	void uart_print_outbuffer(void);
	int uart_checksum(int, int);
	void uart_send_byte(byte);
	void uart_putchar(void);
	void uart_start_snd(void);

#endif

#ifndef _SCHEDULER_H_GUARD_
#define _SCHEDULER_H_GUARD_

#undef EXTERN /*nessesary if more .h files with global declarations are in the code*/
#ifdef SCHEDULER_GLOBAL_DEFENITIONS
#define EXTERN
#else
#define EXTERN extern
#endif

typedef struct telemetryData{
	unsigned int avgLoopTime;
	unsigned int maxLoopTime;
	short MainState;
	unsigned short missedDeadlines;
	short currActualRoll;
	short currActualPich;
	short currActualYaw;
	unsigned short engine1;
	unsigned short engine2;
	unsigned short engine3;
	unsigned short engine4;
}telemetryData;

/*
 * Author: Hajo Kleingeld
 * The enum below contains all possible tasks for the QR, the names should be self explanatory
*/
typedef enum {
				FILTERING,				// Calculates ALL filters until there are no new values available
				SENSOR_LOGGER,			// Logs all values until no more are available
				SEND_LOG,           	// Send all data in the sensor logger
				CALIBRATION,			// Calibrate the Sensors
				YAW_CONTROL,        	// Run yaw control loop
				PITCH_CONTROL,      	// Run pitch control loop
				ROLL_CONTROL,      		// Run Roll control loop
				FULL_CONTROL,			// Run yaw, pitch and roll control loop (= one function)
				ENGINE_CONTROL,     	// Calculate actual engine values
				PROTOCOL,           	// Check for incoming messages (sending is asynchronous)
				PANIC_,					// Task when the program is panicing.
				FINISHED,           	// Finish current schedule, Do time analysis, change schedule/deadline if necessary
				SLEEP                	// Check if the state of the QR is changed and wait until its time to do the next loop
} Tasks;

/**
 * Author: Hajo Kleingeld
 * Several schedules for the several modes.
 */
EXTERN Tasks YawControlMode[] 	= {FILTERING, YAW_CONTROL, PROTOCOL, SENSOR_LOGGER, FINISHED, SLEEP};
EXTERN Tasks FullControlMode[] 	= {FILTERING, FULL_CONTROL, PROTOCOL, SENSOR_LOGGER, FINISHED, SLEEP}; //ENGINE_CONTROL,
EXTERN Tasks ManualMode[] 		= {ENGINE_CONTROL, FILTERING, PROTOCOL, SENSOR_LOGGER, FINISHED, SLEEP};
EXTERN Tasks SafeMode[]			= {FILTERING, PROTOCOL, SENSOR_LOGGER, FINISHED, SLEEP};
EXTERN Tasks PanicMode [] 		= {PANIC_, PROTOCOL, FINISHED, SLEEP};

EXTERN Tasks SendLogMode[] 		= {SEND_LOG, PROTOCOL, FINISHED, SLEEP};
EXTERN Tasks CalibrationMode[]	= {FILTERING, CALIBRATION, FINISHED,SLEEP};

void setSchedule(Tasks * newSchedule);
void resetTimingStatistics(void);
void startProgram(void);

#endif

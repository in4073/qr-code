#include <stdio.h>
#include "main.h"
#include "uart.h"

#ifndef DEBUG_H
#define DEBUG_H

	#define DEBUG_BUFFER_SIZE 		30
	#define DEBUG_MODE_PROTOCOL 	1
	#define DEBUG_MODE_DIRECT 		2
	#define DEBUG_MODE				DEBUG_MODE_PROTOCOL

	//#define debug_printf(...)	message_printf(__VA_ARGS__)

	void debug_printf(const char*, ...);

#endif

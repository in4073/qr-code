#ifndef FIXEDPOINT_H
#define FIXEDPOINT_H

	#define INT_BSIZE 		32 //the type bit size
	//#define INIT_PRECISION 	32 //the precision of newly initiated FixedPoint



	/*arithmatic shift right, for possible negative numbers*/
	#define ASR(N,P)			((N >> P) | ((N < 0) ? (0xffffffff << (32-P)) : 0))

	#define FIXEDPT_ONE			((int)((int)1 << FIXEDPT_FBITS))
	#define FIXEDPT_ONE_HALF 	(FIXEDPT_ONE >> 1)
	#define FIXEDPT_TWO			(FIXEDPT_ONE + FIXEDPT_ONE)
	#define NP					*(int*)		//NP is short for: pointer to n field of FixedPoint struct

	/*
	 * Author: Corniël Joosse
	 * Fixed point initializers and converters
	 * _i versions return an int, not a FixedPoint
	 */
	#define fp_from_int(N, P) 	((N) * (int)(1 << P)) //+ ((N) >= 0 ? 0.5 : -0.5)
	#define fp(N, P) 			fp_cons(fp_from_int(N, P), P)
	#define fp_mask(P) 			((1 << P) - 1)
	#define fp_int(F) 			((F)->n >> (F)->p)
	#define fp_frac(N,P) 		( N & fp_mask(P) )
	#define fp_int_i(N,P) 		(N >> P)
	#define fp_int_si(N,P) 		ASR((int)N,P)

	/*
	 * Author: Corniël Joosse
	 * Faster (*_f) versions of the calculation functions, but no checks, make sure precision is the same.
	 * Also following versions are implemented
	 * 	- constant parameter (*_fc)
	 * 	- using standard ints (*_fi)
	 * 	- especial for signed values (*_fs)
	 * 	- more precise implementation (e.g. for small nr) (*_fp)
	 */
	#define fp_mul_f(R,A,B)		(R)->n = ((A)->n >> ((A)->p/2)) * ((B)->n >> ((A)->p/2))
	#define fp_div_f(R,A,B)		(R)->n = (((A)->n) / (B)->n) << (A)->p
	#define fp_plus_f(R,A,B)	(R)->n = (A)->n + (B)->n
	#define fp_min_f(R,A,B)		(R)->n = (A)->n - (B)->n

	#define fp_div_fc(R,A,C)	(R)->n = (((A)->n) / C)

	#define fp_mul_fsi(A,B,P) 	(ASR((int)A,P/2) * ASR((int)B,P/2))
	#define fp_mul_fsi2(A,B,P) 	 ASR( ((int)A*(int)B), P)
	#define fp_mul_fsi3(A,B,P) 	 ((int)A * ((int)B >> P) )
	#define fp_mul_fi(A,B,P)	(A >> P/2) * (B >> P/2)
	#define fp_div_fi(A,B,P)	((A) / (B)) << P

	#define fp_div_fip(A,B,P)	(((A) << P/2) / (B)) << (P/2);
	#define fp_div_fip2(A,B,P)	(((A) << P) / (B));

	typedef struct fixed_point {
		int n; //number
		int p; //precision
	} FixedPoint;


	FixedPoint fp_cons(int N, int P);
	FixedPoint* fp_mul(FixedPoint* R, FixedPoint* A, FixedPoint* B);
	FixedPoint* fp_div(FixedPoint* R, FixedPoint* A, FixedPoint* B);
	FixedPoint* fp_pow(FixedPoint* R, FixedPoint* A, int pow);
	FixedPoint* fp_plus(FixedPoint* R, FixedPoint* A, FixedPoint* B);
	FixedPoint* fp_min(FixedPoint* R, FixedPoint* A, FixedPoint* B);
	void fp_conv(FixedPoint* A, int precision);
	void fp_print(FixedPoint *A, char *str);

#endif

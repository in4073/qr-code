#ifdef _LAZYTIMER_H_GUARD_
# warning "LazyTimer.h was included twice, this might lead to unexpected results when timing nested functions."
#endif

#ifndef _LAZYTIMER_H_GUARD_
#define _LAZYTIMER_H_GUARD_

#ifdef __linux_DEBUG__
#define X32_ms_clock 1 /*this is a stub*/

#else
#include "../main.h"
#include <x32.h>
#endif

typedef struct LazyTimer{
	unsigned int startTime;
	unsigned int targetTime;
	unsigned int timerRunning;
}LazyTimer;

void initTimer(LazyTimer * timer);
void ResetTimer(LazyTimer * timer);

int TimePassed(LazyTimer * timer);
int runTimer(LazyTimer * timer, int waitTime);

int TimePassed_us(LazyTimer * timer);
int runTimer_us(LazyTimer * timer, int waitTime);

#endif

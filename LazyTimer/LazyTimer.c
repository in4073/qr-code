#include "LazyTimer.h"
#include "../debug.h"

void initTimer(LazyTimer * timer){
	timer->startTime = 0;
	timer->timerRunning = 0;
	timer->targetTime = 0;
}

int runTimer(LazyTimer * timer, int waitTime){
	if(!timer->timerRunning){
		timer->startTime = X32_ms_clock;
		timer->targetTime = timer->startTime + waitTime;
		timer->timerRunning = true;
	}
	else if(X32_ms_clock > timer->targetTime){
		return 1;
	}
	return 0;
}

int TimePassed(LazyTimer * timer){
	if(timer->timerRunning){
		return X32_ms_clock - timer->startTime;
	}
	return 0;
}

void ResetTimer(LazyTimer * timer){
	timer->startTime = 0;
	timer->timerRunning = 0;
	timer->targetTime = 0;
}

int runTimer_us(LazyTimer * timer, int waitTime){
	if(!timer->timerRunning){
		timer->startTime = X32_us_clock;
		timer->targetTime = timer->startTime + waitTime;
		timer->timerRunning = true;
	}
	else if(X32_us_clock > timer->targetTime){
		return 1;
	}
	return 0;
}

int TimePassed_us(LazyTimer * timer){
	if(timer->timerRunning){
		return X32_us_clock - timer->startTime;
	}
	return 0;
}

#ifdef __linux_DEBUG__

int main(void){
	return 0;
}

#endif

#include <x32.h>

#ifndef MAIN_H
#define MAIN_H

	/* define some peripheral short hands
	 */
	#define X32_instruction_counter           peripherals[0x03]

	#define X32_timer_per       peripherals[PERIPHERAL_TIMER1_PERIOD]
	#define X32_leds			peripherals[PERIPHERAL_LEDS]
	#define X32_ms_clock		peripherals[PERIPHERAL_MS_CLOCK]
	#define X32_us_clock		peripherals[PERIPHERAL_US_CLOCK]
	#define X32_QR_a0 			peripherals[PERIPHERAL_XUFO_A0]
	#define X32_QR_a1 			peripherals[PERIPHERAL_XUFO_A1]
	#define X32_QR_a2 			peripherals[PERIPHERAL_XUFO_A2]
	#define X32_QR_a3 			peripherals[PERIPHERAL_XUFO_A3]
	#define X32_QR_s0 			peripherals[PERIPHERAL_XUFO_S0]
	#define X32_QR_s1 			peripherals[PERIPHERAL_XUFO_S1]
	#define X32_QR_s2 			peripherals[PERIPHERAL_XUFO_S2]
	#define X32_QR_s3 			peripherals[PERIPHERAL_XUFO_S3]
	#define X32_QR_s4 			peripherals[PERIPHERAL_XUFO_S4]
	#define X32_QR_s5 			peripherals[PERIPHERAL_XUFO_S5]
	#define X32_QR_timestamp 	peripherals[PERIPHERAL_XUFO_TIMESTAMP]

	#define X32_rs232_data		peripherals[PERIPHERAL_PRIMARY_DATA]
	#define X32_rs232_stat		peripherals[PERIPHERAL_PRIMARY_STATUS]
	#define X32_rs232_char		(X32_rs232_stat & 0x02)
	#define X32_rs232_write		(X32_rs232_stat & 0x01)

	#define X32_wireless_data	peripherals[PERIPHERAL_WIRELESS_DATA]
	#define X32_wireless_stat	peripherals[PERIPHERAL_WIRELESS_STATUS]
	#define X32_wireless_char	(X32_wireless_stat & 0x02)

	#define X32_button			peripherals[PERIPHERAL_BUTTONS]
	#define X32_switches		peripherals[PERIPHERAL_SWITCHES]

	#define word(A, B) 			(uint16_t)((uint16_t)A<<8|(byte)B)
	#define fb(A) 				(byte)((uint16_t)A>>8)
	#define sb(A) 				(byte) A&0xFF

	typedef unsigned char		byte;

	typedef unsigned char		uint8_t;
	typedef unsigned short		uint16_t;
	typedef unsigned int		uint32_t;

	typedef signed char			int8_t;
	typedef short int			int16_t;
	typedef int					int32_t;

	//bolean support
	typedef int 				bool;
	#define true				1;
	#define false				0;

	// led macro's
	#define led_on(N)			X32_leds |= 1<<N
	#define led_off(N)			X32_leds &= ~(1<<N)
	#define led_toggle(N)		X32_leds ^= 1<<N
	#define led_all_on(M)		X32_leds |= M
	#define led_all_off(M)		X32_leds &= ~M
	#define led_all_toggle(M)	X32_leds ^= M

	#define	LED_1S_RATE			0
	#define	LED_PC_COMM			1
	#define	LED_QR_COMM			2
	#define	LED_DEBUG			3 // etc...

#endif

void delay(int ms);

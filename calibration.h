#ifndef CALIBRATION_H
#define CALIBRATION_H
#include "main.h"

#define CAL_NUM_SAMPLES	100

extern int sensor_offsets[];
extern int sensorBaseOffsets[];

void RunCalibration(void);
void print_offsets(void);
bool is_calibrated(void);
#endif

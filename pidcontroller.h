#include "main.h"
#include "fixedpoint.h"
#include "pidtester.h"

#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

	#define PID_PREC		12		//default precision
	#define P2PHI			(int)fp_from_int(0.008,PID_PREC) //(max_gyro/max_acc)*(avg(area of 90 deg movement)/(time of movement * controller frequency)
	#define PID_DT			(int)fp_from_int(1/500,PID_PREC) //dt is now fixed, controller runs at 500Hz
	#define PID_GYRO_SCALAR	6
	#define PID_ACC_SCALAR	4

	//these defines are used to choose between filtered data or direct sensor data
	#define FILTER_DATA		0
	#define SENSOR_DATA		1
	#define DATA_SRC		FILTER_DATA //SENSOR_DATA

	#if DATA_SRC ==  SENSOR_DATA
		#define FILTER_VAL(X,s)		(sensors[X][SensorWriteIndex] - sensor_offsets[X]) * s
	#else
		#define FILTER_VAL(X,s)		filterOutput[X][filterReadIndex[X]] * s //fp_mul_* not needed, s is not fixed point
	#endif


	void pid_set_params(int, int, int, int, int);
	void pid_update_engines(void);
	void pid_yaw_control(void);
	void pid_full_control(void);

	static void _pid_yaw_controller(void);
	static void _pid_pitch_controller(void);
	static void _pid_roll_controller(void);

	extern short cyaw, cpitch, croll, clift;

#endif
